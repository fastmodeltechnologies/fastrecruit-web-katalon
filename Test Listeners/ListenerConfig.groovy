import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Properties.*
import java.lang.String.*
import com.kms.katalon.core.testdata.reader.ExcelFactory


class ListenerConfig {
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def beforeTestCase(TestCaseContext testCaseContext) {
		WebUI.comment("*************** Initializing Before TestSuite ***************")
		FileInputStream reader = new FileInputStream('./config.properties');
		Properties properties = new Properties();
		properties.load(reader);
		String environment = properties.getProperty('environment')
		String profile = properties.getProperty('profile')
		
		switch (environment) {
			
			case "PROD":
			if (properties.getProperty('prodURL') == null) {
			WebUI.comment("***** Launching URL *******" +'https://fastrecruit.fastmodelsports.com/');
			GlobalVariable.environment = 'https://fastrecruit.fastmodelsports.com/'
				
			} else {
				WebUI.comment("***** Launching URL from Prop ******" +properties.getProperty('prodURL'));
				GlobalVariable.environment = properties.getProperty('prodURL');
				}	
				println(properties.getProperty('profile'))
				if (properties.getProperty('profile') == 'NCAAM') {
					GlobalVariable.email = 'qaautomation1@prod.com' 
					GlobalVariable.email2 = 'nbacoachprod@fmsqa.com'
				} else if (properties.getProperty('profile') == 'NCAAW') {
					GlobalVariable.email = 'wncaabcoachprod@fmsqa.com'
				}
				
			break;
			
			case "PROD-BG":
			
			if (properties.getProperty('prodBGURL') == null) {
			WebUI.comment("===Launching URL=== " +'https://fastrecruit-bg.fastmodelsports.com/');
			GlobalVariable.environment = 'https://fastrecruit-bg.fastmodelsports.com/';
			} else {
				WebUI.comment("===Launching URL from Prop=== " +properties.getProperty('prodBGURL'));
				GlobalVariable.environment = properties.getProperty('prodBGURL');
				}
				
				if (properties.getProperty('profile') == 'NCAAM') {
					GlobalVariable.email = 'qaautomation1@prod.com'
					GlobalVariable.email2 = 'nbacoachprod@fmsqa.com'
				} else if (properties.getProperty('profile') == 'NCAAW') {
					GlobalVariable.email = 'wncaabcoachprod@fmsqa.com'
				}
			break;
			
			case "QA":
			
			if (properties.getProperty('qaURL') == null) {
			WebUI.comment("===Launching URL=== " +'https://fastrecruit-qa.fastmodelsports.com/');
			GlobalVariable.environment = 'https://fastrecruit-qa.fastmodelsports.com/'
			} else {
				WebUI.comment("===Launching URL from Prop=== " +properties.getProperty('qaURL'));
				GlobalVariable.environment = properties.getProperty('qaURL');
				}
			
				println(properties.getProperty('profile'))
				if (properties.getProperty('profile') == 'NCAAM') {
					GlobalVariable.email = 'qaautomation1@staging.com'
					GlobalVariable.email2 = 'nbacoachstaging@fmsqa.com'
				} else if (properties.getProperty('profile') == 'NCAAW') {
					GlobalVariable.email = 'qaautomation3@staging.com '
				}
			break;
			
			case "local":
				WebUI.comment("===Launching URL from Prop=== " +properties.getProperty('localURL'));
				GlobalVariable.environment = properties.getProperty('localURL');
			break;

			default:
			WebUI.comment(">>> Unable to find Enviroment Info");
			break;
			
		}	
		Object data = ExcelFactory.getExcelDataWithDefaultSheet("Data Files/fastRecruit_testData.xlsx", profile , true)
		GlobalVariable.data = data
	}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	/*@AfterTestCase
	def afterTestCase(TestCaseContext testCaseContext) {
		WebUI.comment("Test Case Complete")
	}

	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	/*@BeforeTestSuite
	def beforeTestSuite(TestSuiteContext testSuiteContext) {
		
	}

	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestSuite
	def afterTestSuite(TestSuiteContext testSuiteContext) {
		WebUI.comment("Test Suite Complete")
		
	}
}