<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>FeaturesSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e7234dbb-5993-4a39-91a5-8c519845c09b</testSuiteGuid>
   <testCaseLink>
      <guid>82325182-bb77-4315-a15a-b802af4494b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F01_LinkToFastScout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46cb552c-57b6-42e9-8f5c-37bbca533f0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F02_FastConnect</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f43ab08-eeed-4ac0-922f-b968817b396e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F03_ProspectsView</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67bf0675-7836-4e52-841b-81d87844783a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F04_Video</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
