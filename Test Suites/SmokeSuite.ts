<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SmokeSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>40d4b937-42c7-40ef-bc3e-09e10d1c9248</testSuiteGuid>
   <testCaseLink>
      <guid>71ce2071-ab46-40d9-9336-270e52ac281c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SmokeTest/SM01_Add_Archive_DeleteNewRecruit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b9919ff-336c-44cd-87c3-4f07ab2abcc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SmokeTest/SM02_Draft_Board</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adbc7fe6-098f-4df9-aa72-0376e57b5a41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SmokeTest/SM03_Depth_Chart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7014ba39-d9bb-4009-8828-45d5da8e7eda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SmokeTest/SM04_Dashboard_Calendar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c872fc-bb8c-4e65-b10a-e41291cc426e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SmokeTest/SM05_Tasks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf551b4c-c866-44c4-b1e3-42719cfef676</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SmokeTest/SM06_Tourney_Workflow</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
