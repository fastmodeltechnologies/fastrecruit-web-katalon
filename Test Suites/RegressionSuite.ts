<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RegressionSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>28981307-b9b3-4697-854b-0deb363759a9</testSuiteGuid>
   <testCaseLink>
      <guid>c049c33f-ef37-437c-ab19-f2d8457be7cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F01_LinkToFastScout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>083dffc3-fea9-4b1c-8c07-a9c45b0d3ff3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F02_FastConnect</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1af8397-80f6-41e5-b53f-829ecb17e688</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F03_ProspectsView</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>500ea185-c380-4e04-a6f6-22e17ac7b90d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/F04_Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d8e0872-695b-4ccd-9899-3e82f865673f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T123 Recruit Profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aee66c06-ec29-429f-bbe9-e760cb7a8300</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T127 Recruits List Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c42c32c1-9eba-4fec-aa9a-bf328f4b5043</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T128 High School Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>330f73dd-29e1-4358-9dce-05d4eeaa304c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T129 Maps Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4aa4173a-8ab0-4510-93cc-52c4ed093759</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T135 Create FastScout Report</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2eb7fac-a4ab-4c62-8e01-1f396c3f1832</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T136 Transfer Watchlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1922aec6-99cb-44d0-8027-886248dde8a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T138 Recruit Manage Lists</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9c1360d-7869-474b-a225-84b7b992be76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T141 BallerTV</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc7c3cee-3bd0-44f7-90fb-63e52ab38975</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T142 Be The Beast</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c372576a-5706-4ecc-be2e-de914f29f4a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T145 Custom Tourneys</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df0ca0b8-16a2-4547-866f-25a8e6d38304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T152 Schedule ListView</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d356aeba-d1c3-41a3-bc9b-f3eaa3d33cf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegressionTest/FRI-T155 Manage Fields</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
