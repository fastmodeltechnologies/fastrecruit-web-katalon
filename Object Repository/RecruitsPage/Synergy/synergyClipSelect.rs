<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>synergyClipSelect</name>
   <tag></tag>
   <elementGuidId>08507395-4025-4f06-b8ea-d610644ba4e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@class='bg-color-white clips-table hover-bg-color-light-blue-gray helvetica-neue-medium']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
