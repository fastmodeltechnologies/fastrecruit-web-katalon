<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>markTaskCompleteBtn</name>
   <tag></tag>
   <elementGuidId>cf340e17-779b-4b05-ae78-693c033601cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'markTaskCompleted')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'markTaskCompleted')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>markTaskCompleted</value>
   </webElementProperties>
</WebElementEntity>
