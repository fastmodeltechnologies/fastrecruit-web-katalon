<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tasksTab</name>
   <tag></tag>
   <elementGuidId>038f7c97-4bdc-4e10-94ff-93fc2a19d5d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = '#/recruits/tasks']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@href = '#/recruits/tasks']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/recruits/tasks</value>
   </webElementProperties>
</WebElementEntity>
