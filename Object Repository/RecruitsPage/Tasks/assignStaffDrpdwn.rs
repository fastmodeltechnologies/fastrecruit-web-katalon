<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>assignStaffDrpdwn</name>
   <tag></tag>
   <elementGuidId>ed4ede4d-c425-4fdb-8f0e-6491af5ce59e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[5]/div/section/div[2]/div/section/div/div[1]/div/div[2]/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot;Select-placeholder&quot; and contains(text(),'Select Staff')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[5]/div/section/div[2]/div/section/div/div[1]/div/div[2]/div</value>
   </webElementProperties>
</WebElementEntity>
