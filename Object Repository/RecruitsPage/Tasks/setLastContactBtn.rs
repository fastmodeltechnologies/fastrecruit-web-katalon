<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>setLastContactBtn</name>
   <tag></tag>
   <elementGuidId>1c3401a1-c779-480f-a1fb-2ee907aefed9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@class='pd-md']//div//div//a//span[contains(text(),&quot;Set Last Contact&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
