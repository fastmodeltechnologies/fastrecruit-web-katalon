<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deleteTaskBtn</name>
   <tag></tag>
   <elementGuidId>201a5b7a-4fc3-4e5b-a0e2-1be6c0d43ddd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'deleteTasks')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'markTaskCompleted')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>markTaskCompleted</value>
   </webElementProperties>
</WebElementEntity>
