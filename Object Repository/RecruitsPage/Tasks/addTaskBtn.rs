<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addTaskBtn</name>
   <tag></tag>
   <elementGuidId>c1e1b782-2ffc-4df9-9a53-bbf4f83ea9bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'addTaskRecruits']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id = 'addTaskRecruits']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addTaskRecruits</value>
   </webElementProperties>
</WebElementEntity>
