<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectStaffFromList</name>
   <tag></tag>
   <elementGuidId>c5f95400-2dde-4ed9-ae4a-eae3136be5af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[5]/div/section/div[2]/div/section/div/div[1]/div/div[2]/div/div[2]/div/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@role=&quot;option&quot;][contains(text(),'${assignStaff}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[5]/div/section/div[2]/div/section/div/div[1]/div/div[2]/div/div[2]/div/div</value>
   </webElementProperties>
</WebElementEntity>
