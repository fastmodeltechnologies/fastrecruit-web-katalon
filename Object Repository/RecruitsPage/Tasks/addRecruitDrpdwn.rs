<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addRecruitDrpdwn</name>
   <tag></tag>
   <elementGuidId>3efe3d58-30b3-4c9d-b775-ace60d0ab700</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[5]/div/section/div[2]/div/section/div/div[1]/div/div[1]/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot;Select-placeholder&quot; and contains(text(),'Leave blank for a general task')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[5]/div/section/div[2]/div/section/div/div[1]/div/div[1]/div</value>
   </webElementProperties>
</WebElementEntity>
