<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addInstructionTxtFld</name>
   <tag></tag>
   <elementGuidId>bc85cdb6-150c-4dd6-9d43-af2e14ff8dc5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'recruitsInstructions']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id = 'recruitsInstructions']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>recruitsInstructions</value>
   </webElementProperties>
</WebElementEntity>
