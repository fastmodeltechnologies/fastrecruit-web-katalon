<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>editTaskBtn</name>
   <tag></tag>
   <elementGuidId>70cc5a4e-89ff-420a-a867-0b027693552c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'editTasks')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'markTaskCompleted')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>markTaskCompleted</value>
   </webElementProperties>
</WebElementEntity>
