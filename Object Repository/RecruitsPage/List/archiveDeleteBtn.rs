<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>archiveDeleteBtn</name>
   <tag></tag>
   <elementGuidId>cfbeb3a4-92ff-4729-8ab0-ea256f5625bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[contains(@class,'fa fa-trash')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.dropdown-menu.recruit-controls-dropdown > div.pd-sm > span.color-primary.hover-color-primary-darken.lg-font.display-block</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-trash mg-right-md']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-trash mg-right-md</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appContainer&quot;)/div[@class=&quot;flex-grow-1 display-flex flex-direction-column&quot;]/div[3]/div[1]/div[1]/section[@class=&quot;recruit-table-container&quot;]/div[@class=&quot;recruit-list-table display-flex flex-direction-column&quot;]/div[@class=&quot;recruitlist-tbody&quot;]/div[1]/div[1]/table[1]/tbody[1]/tr[@class=&quot;align-items-center no-border-top&quot;]/td[10]/div[1]/span[@class=&quot;dropdown open&quot;]/ul[@class=&quot;dropdown-menu recruit-controls-dropdown&quot;]/div[@class=&quot;pd-sm&quot;]/span[@class=&quot;color-primary hover-color-primary-darken lg-font display-block&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appContainer']/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr/td[10]/div/span/ul/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mac Irvin Fire'])[1]/following::span[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Restore'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Delete']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span/ul/div/span</value>
   </webElementXpaths>
</WebElementEntity>
