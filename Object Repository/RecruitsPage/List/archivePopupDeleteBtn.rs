<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>archivePopupDeleteBtn</name>
   <tag></tag>
   <elementGuidId>f87f3874-334c-4eb7-ab63-234f02408cb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button/i[contains(@class,'fa fa-trash')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[19]/div/section/div[2]/div/footer/div/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[19]/div/section/div[2]/div/footer/div/button[2]</value>
   </webElementProperties>
</WebElementEntity>
