<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recruitNameFromGrid</name>
   <tag></tag>
   <elementGuidId>7af032da-01dd-4579-a434-404a1e01bd65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr/th/div/div[1]/div/div/div/span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'${recruitName}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr/th/div/div[1]/div/div/div/span</value>
   </webElementProperties>
</WebElementEntity>
