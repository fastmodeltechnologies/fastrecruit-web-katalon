<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>arcihivedRecruitNameRow1</name>
   <tag></tag>
   <elementGuidId>084e34ec-2592-4285-aa1e-cec1f9f9109d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//span[@class='color-primary hover-color-primary-darken lg-font'])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr[1]/th/div/div[1]/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr[1]/th/div/div[1]/div/div/div/span</value>
   </webElementProperties>
</WebElementEntity>
