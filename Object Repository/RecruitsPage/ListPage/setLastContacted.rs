<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>setLastContacted</name>
   <tag></tag>
   <elementGuidId>4eccda2d-c00f-4f14-b1e7-377fb686ff5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/span[@class=&quot;color-primary hover-color-primary-darken sm-font&quot;]//div//i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
