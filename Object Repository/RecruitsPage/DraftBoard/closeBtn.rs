<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>closeBtn</name>
   <tag></tag>
   <elementGuidId>70126c24-ed9e-4499-8b6f-4ceefc21c711</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'closeButton')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'closeButton')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>closeButton</value>
   </webElementProperties>
</WebElementEntity>
