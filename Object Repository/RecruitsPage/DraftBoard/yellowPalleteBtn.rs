<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>yellowPalleteBtn</name>
   <tag></tag>
   <elementGuidId>5f4115b0-678e-4295-a48f-8336ebedf8cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@style, 'background-color: rgb(255, 212, 0)')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@style, 'background-color: rgb(255, 212, 0)')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(255, 212, 0)</value>
   </webElementProperties>
</WebElementEntity>
