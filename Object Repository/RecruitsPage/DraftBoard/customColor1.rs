<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>customColor1</name>
   <tag></tag>
   <elementGuidId>e7ea6e17-280a-4883-8e7b-d038e026ae84</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@style, 'background-color: rgb(72, 50, 168)')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@style, 'background-color: rgb(72, 50, 168)')])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(72, 50, 168)</value>
   </webElementProperties>
</WebElementEntity>
