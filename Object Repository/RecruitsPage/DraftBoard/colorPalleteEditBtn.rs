<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>colorPalleteEditBtn</name>
   <tag></tag>
   <elementGuidId>1d8a148a-0781-4cf2-871e-3026cb2580f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-pencil cursor-pointer']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//i[@class = 'fa fa-pencil cursor-pointer']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil cursor-pointer</value>
   </webElementProperties>
</WebElementEntity>
