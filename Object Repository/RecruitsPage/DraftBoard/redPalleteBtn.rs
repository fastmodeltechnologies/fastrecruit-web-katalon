<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>redPalleteBtn</name>
   <tag></tag>
   <elementGuidId>29af66e2-0758-48d6-802e-dec1cd5efb63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@style, 'background-color: rgb(0, 128, 0)')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@style, 'background-color: rgb(0, 128, 0)')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(0, 128, 0)</value>
   </webElementProperties>
</WebElementEntity>
