<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addColorPlusBtn</name>
   <tag></tag>
   <elementGuidId>5efb2684-ee8c-462d-afd5-479f10970eb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-plus']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//i[@class = 'fa fa-plus']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-plus</value>
   </webElementProperties>
</WebElementEntity>
