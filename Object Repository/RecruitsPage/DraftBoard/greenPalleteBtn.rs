<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>greenPalleteBtn</name>
   <tag></tag>
   <elementGuidId>3090b138-dc7a-4824-aadb-17f17aa53df7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@style, 'background-color: rgb(0, 128, 0)')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@style, 'background-color: rgb(0, 128, 0)')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>style</name>
      <type>Main</type>
      <value>background-color: rgb(0, 128, 0)</value>
   </webElementProperties>
</WebElementEntity>
