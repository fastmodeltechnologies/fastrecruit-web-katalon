<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deleteCustomColorBtn</name>
   <tag></tag>
   <elementGuidId>cbe29913-8f1b-4d18-8566-7b4851715542</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div:nth-child(31) > div > section > div.display-flex.justify-content-center > div > section > div.display-flex.flex-wrap-wrap > div:nth-child(4) > svg</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'closeButtonDF']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>closeButtonDF</value>
   </webElementProperties>
</WebElementEntity>
