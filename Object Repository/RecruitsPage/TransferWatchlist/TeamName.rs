<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TeamName</name>
   <tag></tag>
   <elementGuidId>4df45512-93d3-4573-835d-751af32d222d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'Florida Southern']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='Select-placeholder' and contains(text(),'Team')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Florida Southern</value>
   </webElementProperties>
</WebElementEntity>
