<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TransferStatusDrpdwn</name>
   <tag></tag>
   <elementGuidId>b1cb6b2f-b809-46dd-9b53-15c074bc8eb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Player' or . = 'Player') and contains(@class, 'playerDropDown')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Transfer Status' or . = 'Transfer Status') and contains(@class, 'Select-placeholder')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Text</name>
      <type>Main</type>
      <value>Player</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>playerDropDown</value>
   </webElementProperties>
</WebElementEntity>
