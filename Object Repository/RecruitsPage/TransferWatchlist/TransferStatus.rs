<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TransferStatus</name>
   <tag></tag>
   <elementGuidId>b42c5c30-9ba1-4978-91c1-45d9e1dfe827</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'Wes Bongiorni']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@aria-label = '${TransferStatus}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Wes Bongiorni</value>
   </webElementProperties>
</WebElementEntity>
