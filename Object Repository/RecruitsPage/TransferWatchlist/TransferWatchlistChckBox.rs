<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TransferWatchlistChckBox</name>
   <tag></tag>
   <elementGuidId>4b231565-d5a4-4474-963e-3397c7354129</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Link To FastScout' or . = 'Link To FastScout')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//span[@class='pd-left-sm'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;recruit-info&quot;]/div/section/div[9]/div[1]/span/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Link To FastScout</value>
   </webElementProperties>
</WebElementEntity>
