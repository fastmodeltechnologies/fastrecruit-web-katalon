<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>divisionDrpdwn</name>
   <tag></tag>
   <elementGuidId>72c9c79b-0140-485d-8ed5-7580b5b2e552</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Division' or . = 'Division') and contains(@class, 'leagueDropDown')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Division' or . = 'Division') and contains(@class, 'leagueDropDown')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Division</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leagueDropDown</value>
   </webElementProperties>
</WebElementEntity>
