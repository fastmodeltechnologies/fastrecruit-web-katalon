<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>linkPlayerName</name>
   <tag></tag>
   <elementGuidId>f9e3cc23-e18b-44f3-98de-169532dd6a6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'Wes Bongiorni']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@aria-label = '${linkPlayerName}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Wes Bongiorni</value>
   </webElementProperties>
</WebElementEntity>
