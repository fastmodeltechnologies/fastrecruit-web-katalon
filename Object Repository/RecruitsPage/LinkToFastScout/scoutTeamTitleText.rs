<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>scoutTeamTitleText</name>
   <tag></tag>
   <elementGuidId>a3d38eb7-2b0b-440f-abe4-6c1188184315</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//title</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Division' or . = 'Division') and contains(@class, 'leagueDropDown')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Division</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leagueDropDown</value>
   </webElementProperties>
</WebElementEntity>
