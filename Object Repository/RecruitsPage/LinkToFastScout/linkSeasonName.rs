<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>linkSeasonName</name>
   <tag></tag>
   <elementGuidId>4a9daade-5f24-44d4-9e75-8d3acfbc6120</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'Florida Southern']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@aria-label = '${linkSeasonName}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Florida Southern</value>
   </webElementProperties>
</WebElementEntity>
