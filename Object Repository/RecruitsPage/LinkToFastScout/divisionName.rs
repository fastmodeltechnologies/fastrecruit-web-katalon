<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>divisionName</name>
   <tag></tag>
   <elementGuidId>388d22f4-a1b8-4645-bc5e-a6c7c34f29e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'D-II']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@aria-label = '${divisionName}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>D-II</value>
   </webElementProperties>
</WebElementEntity>
