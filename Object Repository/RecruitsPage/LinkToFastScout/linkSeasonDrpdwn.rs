<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>linkSeasonDrpdwn</name>
   <tag></tag>
   <elementGuidId>e423dee9-9313-4f9b-9bcc-9ff2cee7b560</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Player' or . = 'Player') and contains(@class, 'playerDropDown')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Season' or . = 'Season') and contains(@class, 'seasonDropDown ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Text</name>
      <type>Main</type>
      <value>Player</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>playerDropDown</value>
   </webElementProperties>
</WebElementEntity>
