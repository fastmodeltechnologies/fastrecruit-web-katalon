<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fastConnectListDrpdwn</name>
   <tag></tag>
   <elementGuidId>016bb555-9d51-4cf5-8bd9-b766810ecf10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fastConnectRecruits']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@class=&quot;Select-value-label&quot;]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fastConnectRecruits</value>
   </webElementProperties>
</WebElementEntity>
