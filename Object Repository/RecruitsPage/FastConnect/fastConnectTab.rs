<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fastConnectTab</name>
   <tag></tag>
   <elementGuidId>f22ed0e9-13ac-4338-a5fd-6da6509abc7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = '#/recruits/connect']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@href = '#/recruits/connect']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/recruits/connect</value>
   </webElementProperties>
</WebElementEntity>
