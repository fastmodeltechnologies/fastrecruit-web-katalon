<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RedStarIcon</name>
   <tag></tag>
   <elementGuidId>0530cfa6-b756-446a-ad79-7443d88a410d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[@class='fa position-absolute color-white fa-star red-with-white-border']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#seasonDropdown-2021 > i.fa.fa-caret-down.mg-left-sm.hidden-print</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-caret-down mg-left-sm hidden-print</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;seasonDropdown-2021&quot;)/i[@class=&quot;fa fa-caret-down mg-left-sm hidden-print&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='seasonDropdown-2021']/i</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/span/div/i</value>
   </webElementXpaths>
</WebElementEntity>
