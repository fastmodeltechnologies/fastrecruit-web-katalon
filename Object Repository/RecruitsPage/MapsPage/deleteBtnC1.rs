<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deleteBtnC1</name>
   <tag></tag>
   <elementGuidId>36814bf9-822b-4da4-a513-d7c1870027c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id ='draft-delete-C-0']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draft-delete-PG-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draft-delete-PG-0</value>
   </webElementProperties>
</WebElementEntity>
