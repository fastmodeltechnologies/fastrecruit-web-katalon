<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>highSchoolNameLink</name>
   <tag></tag>
   <elementGuidId>f84b38f8-c463-4b37-8636-d11ea24b87dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div > div.display-flex.align-items-center > div.flex-grow-1 > div > div.display-flex.align-items-center > div.flex-grow-4.flex-shrink-1.flex-basis-0 > a > div > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class=&quot;info-datablock&quot;])[4]//a/div/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>SIERRA CANYON SCHOOL</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;recruit-info&quot;)/div[1]/div[1]/section[1]/div[@class=&quot;profile-info-table&quot;]/div[@class=&quot;info-datablock&quot;]/div[@class=&quot;info-values&quot;]/div[1]/div[@class=&quot;display-flex align-items-center&quot;]/div[@class=&quot;flex-grow-1&quot;]/div[1]/div[@class=&quot;display-flex align-items-center&quot;]/div[@class=&quot;flex-grow-4 flex-shrink-1 flex-basis-0&quot;]/a[1]/div[1]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='recruit-info']/div/div/section/div[2]/div[4]/div[2]/div/div/div/div/div/div/a/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HIGH SCHOOL'])[2]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='E-mail'])[1]/following::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chatsworth, CA'])[2]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CLUB TEAM'])[2]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/a/div/div</value>
   </webElementXpaths>
</WebElementEntity>
