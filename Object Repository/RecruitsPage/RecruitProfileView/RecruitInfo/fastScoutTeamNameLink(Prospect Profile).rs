<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fastScoutTeamNameLink(Prospect Profile)</name>
   <tag></tag>
   <elementGuidId>35c11808-0ccc-45ac-9244-890bce4edb6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;recruit-info&quot;]/div/div/section/div[4]/div/div[2]/a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[@target='_blank'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;recruit-info&quot;]/div/div/section/div[4]/div/div[2]/a</value>
      <webElementGuid>5a136168-7d32-4cef-83b1-0eec0bce9d78</webElementGuid>
   </webElementProperties>
</WebElementEntity>
