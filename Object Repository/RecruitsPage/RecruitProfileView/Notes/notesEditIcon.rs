<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>notesEditIcon</name>
   <tag></tag>
   <elementGuidId>c340710e-e313-41be-a89b-571e46a92c21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.color-primary.hover-color-primary-darken.sm-font.color-light-gray > i.fa.fa-pencil</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//tr[@class=&quot;notes-table-row&quot;]//i[@class='fa fa-pencil'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-pencil</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;notes-table&quot;)/tbody[1]/tr[@class=&quot;notes-table-row&quot;]/td[5]/div[1]/span[@class=&quot;color-primary hover-color-primary-darken sm-font color-light-gray&quot;]/i[@class=&quot;fa fa-pencil&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='notes-table']/tbody/tr/td[5]/div/span/i</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[5]/div/span/i</value>
   </webElementXpaths>
</WebElementEntity>
