<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>uploadVideo</name>
   <tag></tag>
   <elementGuidId>91935a92-6d3f-4dc9-b902-565d199564e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'upload-input']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id = 'upload-input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>upload-input</value>
   </webElementProperties>
</WebElementEntity>
