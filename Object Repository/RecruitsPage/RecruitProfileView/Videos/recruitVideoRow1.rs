<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recruitVideoRow1</name>
   <tag></tag>
   <elementGuidId>f8a9f057-ef7d-491b-ba57-ac5a0a30b837</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'recruit-video']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//tr[@class = 'recruit-video'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>recruit-video</value>
   </webElementProperties>
</WebElementEntity>
