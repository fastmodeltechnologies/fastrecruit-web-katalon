<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>videosTab</name>
   <tag></tag>
   <elementGuidId>417f566b-8327-4649-a1e2-0a1c71f386f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'videosTabRecruits')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(@class, 'videosTabRecruits')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>videosTabRecruits</value>
   </webElementProperties>
</WebElementEntity>
