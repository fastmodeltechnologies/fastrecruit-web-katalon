<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>signInBtn</name>
   <tag></tag>
   <elementGuidId>0f842cad-2dc0-40bb-8dcf-f15567616336</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(),'SIGN IN')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div/div[1]/form/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div/div[1]/form/div/button</value>
   </webElementProperties>
</WebElementEntity>
