<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>emailTxtFld</name>
   <tag></tag>
   <elementGuidId>4b51593f-7b17-4e0a-9481-0f5edf278c0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'email']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id = 'email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email</value>
   </webElementProperties>
</WebElementEntity>
