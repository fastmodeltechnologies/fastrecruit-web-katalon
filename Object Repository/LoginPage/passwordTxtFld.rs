<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>passwordTxtFld</name>
   <tag></tag>
   <elementGuidId>486ffaef-f2f0-4bb1-aaf5-b66ab70102b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'password']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id = 'password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
   </webElementProperties>
</WebElementEntity>
