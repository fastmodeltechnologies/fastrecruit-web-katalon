<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addVenue</name>
   <tag></tag>
   <elementGuidId>03035e68-3567-4048-896d-285a6ce4a804</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'Add Venue')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.border-none.border-radius-xxl.bg-color-transparent.color-primary.hover-color-primary-darken.pd-top-sm.pd-right-md.pd-bottom-sm.pd-left-md.cursor-pointer.outline-none.hover-visibility-parent.mg-right-md > span.display-inline-block.md-font.mg-left-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>display-inline-block md-font mg-left-sm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Venue</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;color-text&quot;]/div[15]/div[1]/section[@class=&quot;Modal--container Modal-enter-done&quot;]/div[@class=&quot;display-flex justify-content-center&quot;]/div[@class=&quot;Modal--content lg&quot;]/section[@class=&quot;pd-md&quot;]/form[1]/div[@class=&quot;container-fluid&quot;]/div[1]/div[1]/div[1]/div[1]/div[@class=&quot;CustomTourneyManageModal--grid-container&quot;]/div[@class=&quot;mg-bottom-md btn-group&quot;]/button[@class=&quot;border-none border-radius-xxl bg-color-transparent color-primary hover-color-primary-darken pd-top-sm pd-right-md pd-bottom-sm pd-left-md cursor-pointer outline-none hover-visibility-parent mg-right-md&quot;]/span[@class=&quot;display-inline-block md-font mg-left-sm&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Games'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Venues'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove Selected Venues'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Venue']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/div/div/div/button/span</value>
   </webElementXpaths>
</WebElementEntity>
