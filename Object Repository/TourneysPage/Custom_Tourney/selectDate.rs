<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectDate</name>
   <tag></tag>
   <elementGuidId>9091fdd8-48e4-4310-b90d-5f1f1dd201e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.display-block.color-black.focused-border-shadow.full-width.line-height-1.mg-none.pd-top-sm.pd-bottom-sm.pd-right-md.pd-left-md.resize-none.border-color-primary.border-width-1px.border-style-solid.outline-none.is-focused.bg-color-white.min-width-100.half-width</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='portal-root']//following::select[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>01246295-1a5f-467d-9616-3ef2a15a1657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>display-block color-black focused-border-shadow full-width line-height-1 mg-none pd-top-sm pd-bottom-sm pd-right-md pd-left-md resize-none border-color-primary border-width-1px border-style-solid outline-none is-focused bg-color-white min-width-100 half-width</value>
      <webElementGuid>afb41d91-01c4-4d6a-8e15-0a741ac2442c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Date</value>
      <webElementGuid>5a5a163d-9224-4061-bb46-55c60fc48d99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DateTue 6/1Wed 6/2Thu 6/3Fri 6/4Sat 6/5Sun 6/6Mon 6/7Tue 6/8Wed 6/9Thu 6/10Fri 6/11Sat 6/12Sun 6/13</value>
      <webElementGuid>2d221155-a9e8-4a8e-b4db-7d803718f9c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;color-text&quot;]/div[15]/div[1]/section[@class=&quot;Modal--container Modal-enter-done&quot;]/div[@class=&quot;display-flex justify-content-center&quot;]/div[@class=&quot;Modal--content sm&quot;]/section[@class=&quot;pd-md&quot;]/form[1]/div[1]/div[@class=&quot;display-flex flex-direction-row full-width mg-top-xs&quot;]/select[@class=&quot;display-block color-black focused-border-shadow full-width line-height-1 mg-none pd-top-sm pd-bottom-sm pd-right-md pd-left-md resize-none border-color-primary border-width-1px border-style-solid outline-none is-focused bg-color-white min-width-100 half-width&quot;]</value>
      <webElementGuid>334dac34-cdd7-4b85-b8f4-66f2b26fbadd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/following::select[1]</value>
      <webElementGuid>eccfe168-9834-45a0-8bf4-e2fb82850969</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[7]/preceding::select[2]</value>
      <webElementGuid>2394ac2b-94c7-46a5-8c30-dfc617e0a151</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/select</value>
      <webElementGuid>fbe05887-138e-467e-80fe-e594675794ca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
