<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>color</name>
   <tag></tag>
   <elementGuidId>850b8880-4ff3-4e88-8446-24271c2544b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@title='#AB149E']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div[title=&quot;#AB149E&quot;] > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;color-text&quot;]/div[15]/div[1]/section[@class=&quot;Modal--container Modal-enter-done&quot;]/div[@class=&quot;display-flex justify-content-center&quot;]/div[@class=&quot;Modal--content sm&quot;]/section[@class=&quot;pd-md&quot;]/form[1]/div[1]/div[@class=&quot;display-flex mg-top-md&quot;]/div[@class=&quot;display-inline-block&quot;]/div[1]/div[2]/div[@class=&quot;compact-picker&quot;]/div[1]/span[36]/div[1]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[36]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
