<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>assignRecruitsBtn</name>
   <tag></tag>
   <elementGuidId>50b8c44d-6a42-48cb-97ed-160b5eb537bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'assignRecurits']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
   </webElementProperties>
</WebElementEntity>
