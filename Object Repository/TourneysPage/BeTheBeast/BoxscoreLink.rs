<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BoxscoreLink</name>
   <tag></tag>
   <elementGuidId>40bb2d4d-2f5f-4b12-b62f-0bec57d14131</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class='text-center'])[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
   </webElementProperties>
</WebElementEntity>
