<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTBImgSchedulePage</name>
   <tag></tag>
   <elementGuidId>bc1a2163-8c4e-43a9-b5bd-7067f478405c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'4/16/2021')]/following::td[10]//*[@class='mg-right-sm cursor-pointer']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
   </webElementProperties>
</WebElementEntity>
