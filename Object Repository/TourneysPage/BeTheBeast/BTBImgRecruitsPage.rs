<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTBImgRecruitsPage</name>
   <tag></tag>
   <elementGuidId>c099e42d-0064-46c2-a3c4-c753dade96f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value> (//a[@class='mg-right-sm cursor-pointer' and contains(text(),events.bethebeast.com)])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
      <webElementGuid>ed369b7b-857f-4be1-ba58-960d66ba1818</webElementGuid>
   </webElementProperties>
</WebElementEntity>
