<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ExpandIcon</name>
   <tag></tag>
   <elementGuidId>14d3973f-73eb-4801-98bf-141b2f7a633c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[text()='AZ Select 15U ABBY - 16U Boys']/span/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/section/div[3]/div/div/div/div/div/table/tbody/tr[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/section/div[3]/div/div/div/div/div/table/tbody/tr[1]</value>
   </webElementProperties>
</WebElementEntity>
