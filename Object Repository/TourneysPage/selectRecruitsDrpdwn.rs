<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectRecruitsDrpdwn</name>
   <tag></tag>
   <elementGuidId>2e7b5235-f662-4e10-9128-1bc64efcc864</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[18]/div/section/div[2]/div/section/form/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'Select Recruits')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[18]/div/section/div[2]/div/section/form/div</value>
   </webElementProperties>
</WebElementEntity>
