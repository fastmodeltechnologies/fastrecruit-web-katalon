<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>assignStaffChckBox2</name>
   <tag></tag>
   <elementGuidId>292590a2-9368-4da3-a0dd-713fa515e64b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[16]/div/section/div[2]/div/section/form/div/div/div/label/input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='checkbox'])</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[16]/div/section/div[2]/div/section/form/div/div/div/label/input</value>
   </webElementProperties>
</WebElementEntity>
