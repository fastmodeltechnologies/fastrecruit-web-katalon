<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BallerTvImgHighSchoolPage</name>
   <tag></tag>
   <elementGuidId>ed272b12-87ef-41fa-a045-a424fb276abf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[@src='https://download.fastmodeltechnologies.com/fastrecruit/ballertv_logo.png']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
   </webElementProperties>
</WebElementEntity>
