<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BallerTvImgSchedulePage</name>
   <tag></tag>
   <elementGuidId>6e022eb8-590f-40e5-8c8b-00b50ef3f055</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'6/4/2020')]/following::td[10]//*[@class='mg-right-sm cursor-pointer']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
   </webElementProperties>
</WebElementEntity>
