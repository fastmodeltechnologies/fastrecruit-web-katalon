<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BallerTvImgRecruitsPage</name>
   <tag></tag>
   <elementGuidId>4d049776-46e1-4995-9651-c444f8025c67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'assignRecurits']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//*[@class='mg-right-sm cursor-pointer']//img[@src='https://download.fastmodeltechnologies.com/fastrecruit/ballertv_icon.png'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignRecurits</value>
      <webElementGuid>a721ce36-8e0f-4b8c-9ccc-330a9b59a4b9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
