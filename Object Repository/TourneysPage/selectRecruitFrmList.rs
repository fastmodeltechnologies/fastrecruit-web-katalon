<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectRecruitFrmList</name>
   <tag></tag>
   <elementGuidId>bf3469bb-1dd5-4ba6-85c5-78024f95c959</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[18]/div/section/div[2]/div/section/form/div/div[2]/div/div[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'${recruitName}') and (@role='option')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[18]/div/section/div[2]/div/section/form/div/div[2]/div/div[1]</value>
   </webElementProperties>
</WebElementEntity>
