<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tourneyListDrpdwn</name>
   <tag></tag>
   <elementGuidId>b85903c5-073b-4fa8-9611-68553b54dd00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@placeholder = 'Select Session...']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@placeholder = 'Select Session...']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Select Session...</value>
   </webElementProperties>
</WebElementEntity>
