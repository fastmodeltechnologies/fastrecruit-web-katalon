<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deleteRecruitBtn</name>
   <tag></tag>
   <elementGuidId>45636840-bd73-458f-994a-b7f15092c815</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-trash fa-lg display-inline-block vertical-align-middle']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//i[contains(@class,'fa fa-trash')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-trash fa-lg display-inline-block vertical-align-middle</value>
   </webElementProperties>
</WebElementEntity>
