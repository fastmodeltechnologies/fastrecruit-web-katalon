<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectTourneyTeamDrpdwn</name>
   <tag></tag>
   <elementGuidId>3d4794af-a92e-47f2-b5c0-4cfd0bb7e7cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'tourneyTeamSelect')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'tourneyTeamSelect')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tourneyTeamSelect</value>
   </webElementProperties>
</WebElementEntity>
