<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>assignStaffEditBtn</name>
   <tag></tag>
   <elementGuidId>e3aac9ff-5ecd-464d-bf1c-d0ac7ef1bc00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/section/div[3]/div/div/div[1]/div/h4/span[2]/div/button[1]/i</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//i[@class=&quot;fa fa-pencil fa-1x display-inline-block vertical-align-middle&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/section/div[3]/div/div/div[1]/div/h4/span[2]/div/button[1]/i</value>
   </webElementProperties>
</WebElementEntity>
