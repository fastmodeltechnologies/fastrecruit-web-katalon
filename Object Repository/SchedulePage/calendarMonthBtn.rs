<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarMonthBtn</name>
   <tag></tag>
   <elementGuidId>debd17e7-d5c8-49fd-92ca-8701b714ce3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='react-calendar__tile react-calendar__year-view__months__month']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
   </webElementProperties>
</WebElementEntity>
