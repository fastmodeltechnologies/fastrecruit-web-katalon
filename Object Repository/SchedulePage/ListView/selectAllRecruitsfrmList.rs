<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectAllRecruitsfrmList</name>
   <tag></tag>
   <elementGuidId>128f4da2-7eaf-4780-b7ac-b6da1760f591</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='Select-placeholder'][contains(text(),'All Recruits')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
   </webElementProperties>
</WebElementEntity>
