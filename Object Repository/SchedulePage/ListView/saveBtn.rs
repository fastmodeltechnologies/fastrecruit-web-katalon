<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>saveBtn</name>
   <tag></tag>
   <elementGuidId>af46cbeb-3ee3-4f1a-9003-77b76b97a69a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//footer[contains(@class,'recruit-profile-footer edit')])/span/following::a[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
   </webElementProperties>
</WebElementEntity>
