<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tourneyDrpdwn</name>
   <tag></tag>
   <elementGuidId>b973778b-6549-40b6-b98f-baee583774ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value> //div[@class=&quot;Select-placeholder&quot;][contains(text(),'Tourney')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
   </webElementProperties>
</WebElementEntity>
