<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FollowTeamsIcon</name>
   <tag></tag>
   <elementGuidId>cf708494-9dd1-4dea-90a2-a0156508e0ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//i[contains(@class,'fa fa-bookmark')])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div[2]/div[1]/div/table/tbody/tr[1]</value>
   </webElementProperties>
</WebElementEntity>
