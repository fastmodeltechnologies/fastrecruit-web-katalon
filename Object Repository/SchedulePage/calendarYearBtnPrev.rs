<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarYearBtnPrev</name>
   <tag></tag>
   <elementGuidId>54cdfaf0-eabb-472a-bf78-6f5f846dd45a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='react-calendar__navigation__arrow react-calendar__navigation__prev-button']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
   </webElementProperties>
</WebElementEntity>
