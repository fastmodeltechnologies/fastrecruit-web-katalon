<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarYearFutureBtn</name>
   <tag></tag>
   <elementGuidId>2b1392af-cc04-453b-bb4c-a90ebd11fe52</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='react-calendar__navigation__arrow react-calendar__navigation__next-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
