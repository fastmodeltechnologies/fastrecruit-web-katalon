<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarToIcn</name>
   <tag></tag>
   <elementGuidId>61071c28-6f81-467c-806c-91b7fe2d2313</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//i[@class='fa fa-calendar'])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
   </webElementProperties>
</WebElementEntity>
