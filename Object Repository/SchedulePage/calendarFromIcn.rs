<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarFromIcn</name>
   <tag></tag>
   <elementGuidId>8170d791-955a-4af3-8ae3-de5c371872be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//i[@class='fa fa-calendar'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
   </webElementProperties>
</WebElementEntity>
