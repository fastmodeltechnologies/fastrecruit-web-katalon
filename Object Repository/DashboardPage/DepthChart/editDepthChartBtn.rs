<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>editDepthChartBtn</name>
   <tag></tag>
   <elementGuidId>b0d3679e-2f98-4747-ab7b-d91e611c439e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id = 'depthChart-EDIT']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'depthChart-EDIT']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>depthChart-EDIT</value>
   </webElementProperties>
</WebElementEntity>
