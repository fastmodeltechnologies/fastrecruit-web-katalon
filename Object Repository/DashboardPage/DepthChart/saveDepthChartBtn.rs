<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>saveDepthChartBtn</name>
   <tag></tag>
   <elementGuidId>2df1d258-5045-4f70-902b-56cfba8b0991</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id = 'depthChart-SAVE']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'depthChart-SAVE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>depthChart-SAVE</value>
   </webElementProperties>
</WebElementEntity>
