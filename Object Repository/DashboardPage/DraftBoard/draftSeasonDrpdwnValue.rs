<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>draftSeasonDrpdwnValue</name>
   <tag></tag>
   <elementGuidId>9285690b-c4c0-46da-92c3-21035c15d5a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(),'${seasonDrpDwn}')]/parent::div[@class='cursor-pointer'])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'year-3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>year-3</value>
   </webElementProperties>
</WebElementEntity>
