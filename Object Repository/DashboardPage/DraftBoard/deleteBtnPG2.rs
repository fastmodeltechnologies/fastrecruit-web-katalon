<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deleteBtnPG2</name>
   <tag></tag>
   <elementGuidId>56dbf125-b851-448f-b008-5a9a8fe43d16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'draft-delete-PG-1']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draft-delete-PG-1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draft-delete-PG-1</value>
   </webElementProperties>
</WebElementEntity>
