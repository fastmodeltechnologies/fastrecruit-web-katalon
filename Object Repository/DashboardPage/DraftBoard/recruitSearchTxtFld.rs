<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recruitSearchTxtFld</name>
   <tag></tag>
   <elementGuidId>3ece45b6-c50e-4ba1-a978-be6bca007a59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id = 'draftBoardSearch']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draftBoardSearch']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draftBoardSearch</value>
   </webElementProperties>
</WebElementEntity>
