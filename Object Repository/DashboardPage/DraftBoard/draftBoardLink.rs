<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>draftBoardLink</name>
   <tag></tag>
   <elementGuidId>fc6dd0df-0592-434e-aa2f-6a84156f780e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a/h4[contains(text(),'DRAFT BOARD')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div[2]/div[1]/div[1]/div/div[1]/a/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div[2]/div[1]/div[1]/div/div[1]/a/h4</value>
   </webElementProperties>
</WebElementEntity>
