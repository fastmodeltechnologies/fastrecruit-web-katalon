<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1_PG cell on Draft Board on Dashboard</description>
   <name>blankPGMouseOver</name>
   <tag></tag>
   <elementGuidId>1d5ad8e5-2093-4ac5-9e3a-59f5a0d50409</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'drftBoarddb-PGJPG')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'drftBoarddb-PGJPG')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>drftBoarddb-PGJPG</value>
   </webElementProperties>
</WebElementEntity>
