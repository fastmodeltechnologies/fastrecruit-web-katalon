<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deleteBtnPG1</name>
   <tag></tag>
   <elementGuidId>b6b4ad63-7549-4b2b-be80-195bf03ca079</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'draft-delete-PG-0']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draft-delete-PG-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draft-delete-PG-0</value>
   </webElementProperties>
</WebElementEntity>
