<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>seasonDrpdwnBtn</name>
   <tag></tag>
   <elementGuidId>ce38b401-75e1-436f-8f18-3e82b3f84858</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draftBoardSeasonDropdown']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/ul/li/a/i[@class='fa fa-caret-down mg-left-sm hidden-print']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draftBoardSeasonDropdown</value>
      <webElementGuid>22fe32ef-a7ad-43e0-affe-f743a36af599</webElementGuid>
   </webElementProperties>
</WebElementEntity>
