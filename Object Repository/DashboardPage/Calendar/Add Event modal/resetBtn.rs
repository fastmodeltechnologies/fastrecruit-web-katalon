<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>resetBtn</name>
   <tag></tag>
   <elementGuidId>4d0c2e7f-9724-4bda-ac82-65d06a50af74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Reset')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'eventModelSave']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>eventModelSave</value>
   </webElementProperties>
</WebElementEntity>
