<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>saveBtn</name>
   <tag></tag>
   <elementGuidId>f4215284-824a-4055-bc7d-53481a72afc6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id = 'eventModelSave']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'eventModelSave']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>eventModelSave</value>
   </webElementProperties>
</WebElementEntity>
