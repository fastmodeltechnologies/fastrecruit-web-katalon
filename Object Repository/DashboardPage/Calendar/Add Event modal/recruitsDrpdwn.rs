<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recruitsDrpdwn</name>
   <tag></tag>
   <elementGuidId>722650d7-3b71-49a1-ae2a-9e2d5a3faade</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'addEventRecruits')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'addEventRecruits')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>addEventRecruits</value>
   </webElementProperties>
</WebElementEntity>
