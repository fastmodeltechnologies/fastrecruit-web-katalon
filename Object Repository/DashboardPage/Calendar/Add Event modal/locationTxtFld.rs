<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>locationTxtFld</name>
   <tag></tag>
   <elementGuidId>89d779c3-d66b-4bbd-88b5-6f5833d73124</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id = 'addEventLocation']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'addEventLocation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addEventLocation</value>
   </webElementProperties>
</WebElementEntity>
