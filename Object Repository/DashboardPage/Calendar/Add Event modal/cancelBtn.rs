<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cancelBtn</name>
   <tag></tag>
   <elementGuidId>e4db9ae9-5c48-4746-97bc-f182dee99861</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'Cancel')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'eventModelSave']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>eventModelSave</value>
   </webElementProperties>
</WebElementEntity>
