<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>gameNoteTxtFld</name>
   <tag></tag>
   <elementGuidId>5f9a0947-1b9e-4e5c-bf70-98d9e2ac222b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder = 'e.g. &quot;Arrive 15 minutes early to meet parents&quot;']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@placeholder = 'e.g. &quot;Arrive 15 minutes early to meet parents&quot;']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>e.g. &quot;Arrive 15 minutes early to meet parents&quot;</value>
   </webElementProperties>
</WebElementEntity>
