<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>eventNameTxtFld</name>
   <tag></tag>
   <elementGuidId>055f226a-373d-46bd-bb86-7137b46e920f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder = 'e.g. &quot;Centennial High vs. Central HS&quot;']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@placeholder = 'e.g. &quot;Centennial High vs. Central HS&quot;']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>e.g. &quot;Centennial High vs. Central HS&quot;</value>
   </webElementProperties>
</WebElementEntity>
