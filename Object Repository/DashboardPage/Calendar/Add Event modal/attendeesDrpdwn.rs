<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>attendeesDrpdwn</name>
   <tag></tag>
   <elementGuidId>c438c2cf-5ba3-4feb-aab4-a643c0118e1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'addEventAttendees')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'addEventAttendees')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>addEventAttendees</value>
   </webElementProperties>
</WebElementEntity>
