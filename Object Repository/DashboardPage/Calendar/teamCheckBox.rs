<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>teamCheckBox</name>
   <tag></tag>
   <elementGuidId>c4fa4735-2b08-4c4f-b7a2-768df086f71b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarDropDown-1']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'Team')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarDropDown-1</value>
   </webElementProperties>
</WebElementEntity>
