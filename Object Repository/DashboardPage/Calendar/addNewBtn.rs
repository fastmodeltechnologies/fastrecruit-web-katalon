<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addNewBtn</name>
   <tag></tag>
   <elementGuidId>a8bfc430-12cf-4c3f-875f-90ab7ca7e5f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarAddNew']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'calendarAddNew']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarAddNew</value>
   </webElementProperties>
</WebElementEntity>
