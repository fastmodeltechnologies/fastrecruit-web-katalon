<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>showDrpdwnBtn</name>
   <tag></tag>
   <elementGuidId>0af9ad3b-b25e-4415-98f7-c0be218d3e30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarTypeDropdown']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'calendarTypeDropdown']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarTypeDropdown</value>
   </webElementProperties>
</WebElementEntity>
