<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>myLivePeriodCheckbox</name>
   <tag></tag>
   <elementGuidId>8d3244b6-9c43-44c8-949b-e40534d90e51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'My Live Period')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarDropDown-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarDropDown-4</value>
   </webElementProperties>
</WebElementEntity>
