<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarLeftToggleBtn</name>
   <tag></tag>
   <elementGuidId>58b01a05-a829-4f43-a1f2-0185b6ab51a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[@id = 'toggleCalendarLeft']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'toggleCalendarLeft']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>toggleCalendarLeft</value>
   </webElementProperties>
</WebElementEntity>
