<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>calendarRightToggleBtn</name>
   <tag></tag>
   <elementGuidId>a15f28a8-8b9a-4184-b5db-bae365aaa7ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[@id = 'toggleCalendarRight']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'toggleCalendarRight']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>toggleCalendarRight</value>
   </webElementProperties>
</WebElementEntity>
