<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tasksCheckBox</name>
   <tag></tag>
   <elementGuidId>fd2b3885-c1b1-4e33-8fb9-e0fed6ff77da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarDropDown-2']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'Tasks')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarDropDown-2</value>
   </webElementProperties>
</WebElementEntity>
