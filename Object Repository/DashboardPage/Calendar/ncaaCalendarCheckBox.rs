<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ncaaCalendarCheckBox</name>
   <tag></tag>
   <elementGuidId>a3218106-b12f-4289-b29b-26bfe7511e23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarDropDown-3']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'NCAA Calendar')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarDropDown-3</value>
   </webElementProperties>
</WebElementEntity>
