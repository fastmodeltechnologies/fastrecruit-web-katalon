<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>myCalendarCheckBox</name>
   <tag></tag>
   <elementGuidId>a828891d-0d5f-4341-88bd-6b6897d27355</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'calendarDropDown-0']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'My Calendar')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>calendarDropDown-0</value>
   </webElementProperties>
</WebElementEntity>
