<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ratingName</name>
   <tag></tag>
   <elementGuidId>017e2241-5766-4318-959b-0a899f2e3033</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'D-II']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@aria-label = '${ratingName}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>D-II</value>
   </webElementProperties>
</WebElementEntity>
