<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ratingDrpdwn</name>
   <tag></tag>
   <elementGuidId>20ce9cd0-07b9-4fcf-a802-d79b763ba1ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@id,&quot;react-select&quot;)]/div[@class=&quot;Select-input&quot;]/input)[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Division' or . = 'Division') and contains(@class, 'leagueDropDown')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Division</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leagueDropDown</value>
   </webElementProperties>
</WebElementEntity>
