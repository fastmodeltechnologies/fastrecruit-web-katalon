<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>statusName</name>
   <tag></tag>
   <elementGuidId>43ca2d2c-914f-4c05-9d14-8b6573aae23a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'D-II']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@aria-label = '${statusName}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>D-II</value>
   </webElementProperties>
</WebElementEntity>
