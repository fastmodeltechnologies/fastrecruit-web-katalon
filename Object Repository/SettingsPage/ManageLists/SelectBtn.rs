<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectBtn</name>
   <tag></tag>
   <elementGuidId>fb32b2ca-d307-43ef-aa4c-40e5ed6df298</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'Select')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.hover-bg-color-primary.hover-color-white.outline-none.bg-color-white.color-primary.border-width-1px.border-style-solid.border-radius-xxl.border-color-primary.hover-border-color-primary-darken.cursor-pointer.lg-font.pd-top-sm.pd-right-lg.pd-bottom-sm.pd-left-lg > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;color-text&quot;]/div[10]/div[1]/section[@class=&quot;Modal--container Modal-enter-done&quot;]/div[@class=&quot;display-flex justify-content-center&quot;]/div[@class=&quot;Modal--content xs&quot;]/section[@class=&quot;pd-md&quot;]/div[@class=&quot;display-flex align-items-center justify-content-center mg-bottom-xl&quot;]/div[@class=&quot;align-self-center justify-content-space-between&quot;]/a[@class=&quot;hover-bg-color-primary hover-color-white outline-none bg-color-white color-primary border-width-1px border-style-solid border-radius-xxl border-color-primary hover-border-color-primary-darken cursor-pointer lg-font pd-top-sm pd-right-lg pd-bottom-sm pd-left-lg&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='QA Automation1'])[3]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Staff'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ADMIN PORTAL'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a/span</value>
   </webElementXpaths>
</WebElementEntity>
