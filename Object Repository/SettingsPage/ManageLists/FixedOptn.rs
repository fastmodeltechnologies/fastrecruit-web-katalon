<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FixedOptn</name>
   <tag></tag>
   <elementGuidId>ffefe9ee-8e1a-4593-9103-b4fee8108cd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Fixed'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Fixed</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;color-text&quot;]/div[7]/div[1]/section[@class=&quot;Modal--container Modal-enter-done&quot;]/div[@class=&quot;display-flex justify-content-center&quot;]/div[@class=&quot;Modal--content xs&quot;]/section[@class=&quot;pd-md&quot;]/div[@class=&quot;display-flex&quot;]/div[@class=&quot;display-inline-block mg-md flex-basis-50&quot;]/div[@class=&quot;display-flex justify-content-center mg-top-xl mg-bottom-xl&quot;]/div[@class=&quot;align-self-center justify-content-space-between&quot;]/a[@class=&quot;hover-bg-color-primary hover-color-white outline-none bg-color-white color-primary border-width-1px border-style-solid border-radius-xxl border-color-primary hover-border-color-primary-darken cursor-pointer md-font pd-top-sm pd-right-md pd-bottom-sm pd-left-md&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fixed'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rule Based'])[2]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ADMIN PORTAL'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/a/span</value>
   </webElementXpaths>
</WebElementEntity>
