<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SharedListWithIcon</name>
   <tag></tag>
   <elementGuidId>f3849a68-b51b-476b-918c-0f6c9a53a582</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@class=&quot;absolute top-sm right-md&quot;]//i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-share-square.fa-lg.cursor-pointer.color-primary.hover-color-primary.color-dark-gray-lighten.hover-color-dark-gray</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-share-square fa-lg cursor-pointer color-primary hover-color-primary color-dark-gray-lighten hover-color-dark-gray</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;color-text&quot;]/div[9]/div[1]/section[@class=&quot;Modal--container Modal-enter-done&quot;]/div[@class=&quot;display-flex justify-content-center Modal--dialog-aside aside-null&quot;]/div[@class=&quot;Modal--content sm&quot;]/section[@class=&quot;pd-md&quot;]/form[1]/div[1]/div[@class=&quot;display-flex align-items-flex-end&quot;]/div[@class=&quot;flex-shrink-0&quot;]/div[1]/div[@class=&quot;cursor-pointer relative display-block&quot;]/span[@class=&quot;absolute top-sm right-md&quot;]/i[@class=&quot;fa fa-share-square fa-lg cursor-pointer color-primary hover-color-primary color-dark-gray-lighten hover-color-dark-gray&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span/i</value>
   </webElementXpaths>
</WebElementEntity>
