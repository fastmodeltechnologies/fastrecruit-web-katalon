import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 60)
WebUI.click(findTestObject('TopNavigationMenu/Recruits'))
WebUI.click(findTestObject('RecruitsPage/List/addRecruitBtn'))
WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/firstNameTxtFld'), 'Test')
WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastNameTxtFld'), 'Recruit_New001')
WebUI.click(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Test Recruit_New001')
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : 'Test Recruit_New001']), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : 'Test Recruit_New001']))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/tasksBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/addRecruitTaskBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/assignStaffDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/selectStaffFromList',[('assignStaff') : 'QA Automation1']))
WebUI.enhancedClick(findTestObject('Object Repository/SchedulePage/calendarFromIcn'))
WebUI.enhancedClick(findTestObject('Object Repository/SchedulePage/calendarYearFutureBtn'))
WebUI.enhancedClick(findTestObject('Object Repository/DashboardPage/Calendar/calendarDateValue'))
WebUI.setText(findTestObject('RecruitsPage/Tasks/addInstructionTxtFld'), 'FRI-T176 (1.0)-Test Task')
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/saveBtn'))
WebUI.delay(3)
WebUI.verifyElementText(findTestObject('RecruitsPage/Tasks/taskDescriptionTxt'), 'FRI-T176 (1.0)-Test Task', FailureHandling.STOP_ON_FAILURE)
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/editTasksEllipsisBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/markTaskCompleteBtn'))
WebUI.enhancedClick(findTestObject('Object Repository/RecruitsPage/Tasks/setLastContactBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedRecruiterDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedRecruiterSelectFromDrpdwn'))
WebUI.enhancedClick(findTestObject('Object Repository/RecruitsPage/Tasks/methodSelectTypeDrpDwn'))
WebUI.setText(findTestObject('Object Repository/RecruitsPage/Tasks/methodSelectTypeDrpDwn'),'Email')
WebUI.sendKeys(findTestObject('Object Repository/RecruitsPage/Tasks/methodSelectTypeDrpDwn'), Keys.chord(Keys.TAB))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn (1)'))
WebUI.enhancedClick(findTestObject('Object Repository/RecruitsPage/Tasks/showCompletedBtn'))
WebUI.verifyElementText(findTestObject('RecruitsPage/Tasks/taskDescriptionTxt'), 'FRI-T176 (1.0)-Test Task', FailureHandling.STOP_ON_FAILURE)
WebUI.enhancedClick(findTestObject('Object Repository/RecruitsPage/List/closeBtn'))
WebUI.delay(3)
WebUI.verifyElementNotPresent(findTestObject('Object Repository/RecruitsPage/ListPage/setLastContacted'), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveRecruitBtn'))
WebUI.click(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))
WebUI.click(findTestObject('RecruitsPage/List/archiveListBtn'))
WebUI.delay(5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveDeleteBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivePopupDeleteBtn'))
WebUI.closeBrowser()