import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.*
import com.kms.katalon.core.webui.keyword.builtin.VerifyElementPresentKeyword as VerifyElementPresentKeyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('LoginPage/signInBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/selectTransferWatchlistFromList'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/addRecruitBtn'))

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/firstNameTxtFld'), 'Quinn')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastNameTxtFld'), 'Pemberton')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/classTxtFld'), '2023')

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/heightTxtFld'))

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/heightTxtFld'), '69')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/birthdateTxtFld'), '1/2/2005')

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/birthdateLabel'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/TransferWatchlistChckBox'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/divisionDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/divisionName', [('divisionName') : GlobalVariable.data.getValue(
                'divisionName', 3)]))

WebUI.scrollToElement(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonDrpdwn'), 5)

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonName', [('linkSeasonName') : GlobalVariable.data.getValue(
                'linkSeasonName', 14)]))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkTeamDropdown'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkTeamName', [('linkTeamName') : GlobalVariable.data.getValue(
                'linkTeamName', 14)]))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkPlayerDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkPlayerName', [('linkPlayerName') : GlobalVariable.data.getValue(
                'linkPlayerName', 14)]))

WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/classTxtFld'), 'SO')

WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/DateInitiatedTxtFld'), '1/2/2020')

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/TransferStatusDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/TransferStatus', [('TransferStatus') : GlobalVariable.data.getValue(
                'TransferStatus', 14)]))

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))

//Institution Field
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/InsitutionFld'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://fastscout')

WebUI.switchToWindowIndex(1)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/RosterTab'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/AddtoWatchlistBtn'))

WebUI.delay(5)

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.refresh()

WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue(
                'recruitName', 14)]))

//Fastscoutreport Button
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/FastScoutReportBtn'))

WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/PlayerScoutNameTxtFld'), 'PlayerScout1')

WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/SavedTemplateDrpdwn'), '7Player')

WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), Keys.chord(Keys.ENTER))

//Create Player Scout
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/CreateScoutBtn'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

WebUI.delay(20)

assert WebUI.getUrl().contains('https://fastscout')

WebUI.switchToWindowIndex(1)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutReportDoneBtn'))

WebUI.delay(2)

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue(
                'recruitName', 14)]))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/EditRecruitBtn'))

WebUI.sendKeys(findTestObject('RecruitsPage/TransferWatchlist/classTxtFld'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('RecruitsPage/TransferWatchlist/classTxtFld'), Keys.chord(Keys.BACK_SPACE))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteDateInitiated'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteTransferStatus'))

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))

//Stat Icon(Player Page) & Delete PlayerScout
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/StatIconFld'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://fastscout')

WebUI.waitForElementVisible(findTestObject('RecruitsPage/TransferWatchlist/OpponentsTab'), 20)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/logoutCaret'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/logoutBtn'))

WebUI.delay(6)

if (GlobalVariable.environment != 'QA') {
    WebUI.setText(findTestObject('LoginPage/emailTxtFld'), 'qaautomation1scout@prod.com')

    WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), 'FastModel1')
} else {
    WebUI.setText(findTestObject('LoginPage/emailTxtFld'), 'qaautomation1scout@staging.com')

    WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), 'FastModel1')
}

WebUI.enhancedClick(findTestObject('LoginPage/signInBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutsTab'))

WebUI.mouseOver(findTestObject('RecruitsPage/TransferWatchlist/ScoutEllipsisBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutEllipsisBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutArchiveBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ArchivedTab'))

WebUI.mouseOver(findTestObject('RecruitsPage/TransferWatchlist/ArchivedEllipsisBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ArchivedEllipsisBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteScoutBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteScoutPopupBtn'))

WebUI.switchToWindowIndex(0)

//Delete all Recruits
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/ShowAdvancedToolsTxt'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/AllRecruitsChckBox'))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/RecruitsArchiveSelectedBtn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveListBtn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('RecruitsPage/TransferWatchlist/Recruit1ArchiveDeleteBtn'), 10)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/Recruit1ArchiveDeleteBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivePopupDeleteBtn'))

WebUI.delay(8)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('RecruitsPage/TransferWatchlist/Recruit1ArchiveDeleteBtn'), 10)

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/Recruit1ArchiveDeleteBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivePopupDeleteBtn'))

WebUI.delay(2)

WebUI.closeBrowser()

