import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('LoginPage/signInBtn'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Tourneys'))

//BallerTv Tourney
WebUI.enhancedClick(findTestObject('TourneysPage/tourneyListDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyDrpdwnJune2020'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                13)]))

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignStaffBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/AssignNotPresent'))

    WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue(
                    'assignStaff', 9)]))

    WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
}

WebUI.scrollToElement(findTestObject('TourneysPage/assignStaffBtn'), 5)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue('assignStaff', 
                9)]))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(2)

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignRecruitsBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/DeleteAssignRecruit'))

    WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))
}

WebUI.enhancedClick(findTestObject('TourneysPage/assignRecruitsBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitFrmList', [('recruitName') : GlobalVariable.data.getValue(
                'assignRecruit', 20)]))


WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamDrpdwn'))

WebUI.scrollToElement(findTestObject('TourneysPage/selectTourneyTeamFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 13)]), 3)

WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 13)]))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/FollowTeamsIcon'))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/selectTeamName'))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/selectTeamNameFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 13)]))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/selectAllOptn'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/backIcon'))

//Be the Beast Tourney
WebUI.enhancedClick(findTestObject('TourneysPage/tourneyListDrpdwn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('TourneysPage/BeTheBeast/tourneyDrpdwnSpring2021'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                18)]))

WebUI.delay(2)

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignStaffBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/AssignNotPresent'))

    WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue(
                    'assignStaff', 9)]))

    WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
}

WebUI.scrollToElement(findTestObject('TourneysPage/assignStaffBtn'), 5)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue('assignStaff', 
                9)]))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(2)

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignRecruitsBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/DeleteAssignRecruit'))

    WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))
}

WebUI.enhancedClick(findTestObject('TourneysPage/assignRecruitsBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitFrmList', [('recruitName') : GlobalVariable.data.getValue(
                'assignRecruit', 20)]))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamDrpdwn'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('TourneysPage/selectTourneyTeamFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 18)]), 5)

WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 18)]))

WebUI.delay(2)

//BTB Schedule
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Schedule'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(2)

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt'), '4/16/2021')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.ESCAPE))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(2)

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt2'), '12/19/2021')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ESCAPE))

WebUI.waitForElementVisible(findTestObject('TourneysPage/BeTheBeast/BoxscoreLink'), 20)

WebUI.enhancedClick(findTestObject('TourneysPage/BeTheBeast/BoxscoreLink'))

//WebUI.switchToWindowIndex(1)
WebUI.delay(15)

//WebUI.closeWindowIndex(1)
//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.setText(findTestObject('DashboardPage/DraftBoard/recruitSearchTxtFld'), 'Elite')

WebUI.delay(5)

WebUI.sendKeys(findTestObject('DashboardPage/DraftBoard/recruitSearchTxtFld'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('DashboardPage/DraftBoard/recruitSearchTxtFld'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('DashboardPage/DraftBoard/recruitSearchTxtFld'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/rightArrowBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/PaginationDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/select10PerPageFromList'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/sessionDrpdwn'))

WebUI.setText(findTestObject('SchedulePage/ListView/selectsessionfrmList'), '2021 Spring Boys')

WebUI.sendKeys(findTestObject('SchedulePage/ListView/selectsessionfrmList'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SchedulePage/ListView/selectsessionfrmList'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/sessionDrpdwn'))

WebUI.setText(findTestObject('SchedulePage/ListView/selecttourneyfrmList'), 'So Cal Slam')

WebUI.sendKeys(findTestObject('SchedulePage/ListView/selecttourneyfrmList'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SchedulePage/ListView/selecttourneyfrmList'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/assignStaffChckBox2'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/selectRecruitSchedulepage'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))

WebUI.verifyElementPresent(findTestObject('TourneysPage/BeTheBeast/gamesTabRow4'), 5)

WebUI.enhancedClick(findTestObject('TourneysPage/BeTheBeast/BTBImgSchedulePage'))

WebUI.switchToWindowIndex(1)

WebUI.delay(10)

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/ClearMaxClass'))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/ClearSession'))

WebUI.delay(5)

//Ballertv Schedule
WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt'), '6/4/2020')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.ESCAPE))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt2'), '6/17/2020')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ESCAPE))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/selectRecruitSchedulepage'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/BallerTvImgSchedulePage'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://www.ballertv.com')

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Schedule'))

WebUI.delay(3)

//Delete BallerTv tourney
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Tourneys'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyListDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyDrpdwnJune2020'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                13)]))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/FollowTeamsIcon'))

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/selectAllOptn'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.waitForElementVisible(findTestObject('TourneysPage/assignStaffEditBtn'), 30)

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffEditBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/assignStaffChckBox2'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('SchedulePage/ListView/backIcon'))

//Delete BTB tourney
WebUI.enhancedClick(findTestObject('TourneysPage/tourneyListDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/BeTheBeast/tourneyDrpdwnSpring2021'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                18)]))

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffEditBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/assignStaffChckBox2'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))

WebUI.delay(1)

WebUI.closeBrowser()
