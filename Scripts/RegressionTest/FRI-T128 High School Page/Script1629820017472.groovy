import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('Object Repository/LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('Object Repository/LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('Object Repository/LoginPage/signInBtn'))
WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Recruits'))
WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Recruits'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 10))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 10)]), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/HighSchool/highSchoolName', [('highSchoolName') : GlobalVariable.data.getValue(
                'highSchoolName', 10)]), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.verifyElementPresent(findTestObject('RecruitsPage/HighSchool/upcomingGamesTxt'), 0)
WebUI.verifyElementPresent(findTestObject('RecruitsPage/HighSchool/pastGamesTxt'), 0)
WebUI.enhancedClick(findTestObject('RecruitsPage/HighSchool/addressFld'))
WebUI.delay(5)
WebUI.back()
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('RecruitsPage/HighSchool/nfhsImg1'))
WebUI.switchToWindowIndex(1)
WebUI.enableSmartWait()
assert WebUI.getUrl().contains('https://www.nfhsnetwork.com')
//Switch to original
WebUI.switchToWindowIndex(0)
WebUI.enhancedClick(findTestObject('RecruitsPage/HighSchool/nfhsImg2'))
WebUI.switchToWindowIndex(1)
WebUI.enableSmartWait()
assert WebUI.getUrl().contains('https://www.nfhsnetwork.com/')
WebUI.switchToWindowIndex(0)
WebUI.enableSmartWait()
WebUI.closeBrowser()