import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.*
import com.kms.katalon.core.webui.keyword.builtin.VerifyElementPresentKeyword as VerifyElementPresentKeyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('Object Repository/LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/LoginPage/signInBtn'))

WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Recruits'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/MapsBtn'))

//White Circle
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Chicago,IL,USA')

WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/WhiteCircleIcon'))

//Red Circle
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Mouth of Wilson, VA, USA')

WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/DateDrpDwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/SelectOct1'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/SelectOct31'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/RedCircleIcon'))

//white star
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Dashboard'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/seasonDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftSeasonDrpdwnValue', [('seasonDrpDwn') : (GlobalVariable.data).getValue(
                'seasonDrpDwn', 6)]))

WebUI.mouseOver(findTestObject('RecruitsPage/MapsPage/blankCMouseOver'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/blankCMouseOver'))

WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/selectPlayerFromDrpdwn', [('recruitName') : (GlobalVariable.data).getValue('recruitName', 17)]))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Recruits'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/MapsBtn'))

WebUI.delay(5)

WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Mouth of Wilson, VA, USA')

WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/WhiteStarIcon'))

WebUI.delay(2)

//Red Star
WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/DateDrpDwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/SelectOct1'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/SelectOct31'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/RedStarIcon'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Dashboard'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('RecruitsPage/MapsPage/draftBoardC1'))

WebUI.enhancedClick(findTestObject('RecruitsPage/MapsPage/deleteBtnC1'))

WebUI.delay(2)

WebUI.closeBrowser()

