import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.*
import com.kms.katalon.core.webui.keyword.builtin.VerifyElementPresentKeyword as VerifyElementPresentKeyword

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('LoginPage/signInBtn'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Tourneys'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyListDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyDrpdwnJune2020'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                13)]))

WebUI.delay(2)

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignStaffBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/AssignNotPresent'))

    WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue(
                    'assignStaff', 9)]))

    WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
}

WebUI.scrollToElement(findTestObject('TourneysPage/assignStaffBtn'), 5)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue('assignStaff', 
                9)]), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignRecruitsBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/DeleteAssignRecruit'))

    WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))
}

WebUI.enhancedClick(findTestObject('TourneysPage/assignRecruitsBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitFrmList', [('recruitName') : GlobalVariable.data.getValue(
                'assignRecruit', 10)]))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 13)]))

WebUI.enhancedClick(findTestObject('TourneysPage/MyRecruitGamesTab'))

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('TourneysPage/gamesTabRow1'), 5)

//BallerTv
WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/BallerTvImgTourneysPage'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://www.ballertv.com')

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Schedule'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt'), '6/4/2020')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt'), Keys.chord(Keys.ESCAPE))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt2'), '6/17/2020')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ESCAPE))

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/assignStaffChckBox2'))

WebUI.verifyElementPresent(findTestObject('SchedulePage/schedueListRow1'), 5)

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/BallerTvImgSchedulePage'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://www.ballertv.com')

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))

WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 11))
WebUI.delay(1)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue(
                'recruitName', 11)]))
WebUI.enhancedClick(findTestObject('Object Repository/RecruitsPage/RecruitProfileView/additionalEventsBtn'))
WebUI.enableSmartWait()
WebUI.delay(5)
WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/BallerTvImgRecruitsPage'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://www.ballertv.com/')

WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))

WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'HUNTINGTON')

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/highSchoolName2', [('highSchoolName') : GlobalVariable.data.getValue(
                'highSchoolName', 13)]), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/BallerTvImgHighSchoolPage'))

WebUI.switchToWindowIndex(1)

WebUI.enableSmartWait()

assert WebUI.getUrl().contains('https://www.ballertv.com/')

//Switch to original
WebUI.switchToWindowIndex(0)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Tourneys'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyListDrpdwn'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyDrpdwnJune2020'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                13)]))

WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffEditBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/assignStaffChckBox2'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))

WebUI.delay(1)

WebUI.closeBrowser()

