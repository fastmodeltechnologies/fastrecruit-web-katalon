import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue(
                'recruitName', 15)]))
//Fastscoutreport Button
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/FastScoutReportBtn'))
WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/PlayerScoutNameTxtFld'), 'FastScout1')
WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/SavedTemplateDrpdwn'), '7Player')
WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), Keys.chord(Keys.ENTER))

//Create Player Scout
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/CreateScoutBtn'))
WebUI.switchToWindowIndex(1)
WebUI.enableSmartWait()
assert WebUI.getUrl().contains('https://fastscout')
WebUI.switchToWindowIndex(1)
WebUI.waitForElementVisible(findTestObject('RecruitsPage/TransferWatchlist/ScoutReportDoneBtn'), 60)
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutReportDoneBtn'))
WebUI.delay(2)
WebUI.closeWindowIndex(1)

//Switch to original
WebUI.switchToWindowIndex(0)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 10))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 10)]), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue(
                'recruitName', 10)]))
WebUI.verifyElementNotPresent(findTestObject('RecruitsPage/TransferWatchlist/FastScoutReportBtn'), 20)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/ShowAdvancedToolsTxt'))
//WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/ALLDrpdwnBtn'))
//WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftSeasonDrpdwnValue', [('seasonDrpDwn') : GlobalVariable.data.getValue( 'seasonDrpDwn', 15)]))
//WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/RecruitsChckBox', [('recruitName') : findTestData(GlobalVariable.data).getValue('recruitName', 15)]))
WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.END, Keys.SHIFT, Keys.ARROW_UP, Keys.BACK_SPACE))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 15))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 3)]), 15)
WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/RecruitsChckBox', [('recruitName') : GlobalVariable.data.getValue( 'recruitName', 15)]))
//Fastscoutreport Button
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/FastScoutReportBtn'))
WebUI.setText(findTestObject('RecruitsPage/TransferWatchlist/PlayerScoutNameTxtFld'), 'FastScout2')

//Create Player Scout
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/CreateScoutBtn'))
WebUI.delay(3)
WebUI.switchToWindowIndex(1)
WebUI.enableSmartWait()
assert WebUI.getUrl().contains('https://fastscout')
WebUI.switchToWindowIndex(1)
WebUI.waitForElementVisible(findTestObject('RecruitsPage/TransferWatchlist/ScoutReportDoneBtn'), 60)
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutReportDoneBtn'))
WebUI.delay(2)
WebUI.waitForElementVisible(findTestObject('RecruitsPage/TransferWatchlist/OpponentsTab'), 20)
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/logoutCaret'))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/logoutBtn'))
WebUI.delay(4)

if (GlobalVariable.environment != 'QA') {
	
	WebUI.setText(findTestObject('LoginPage/emailTxtFld'), 'qaautomation1scout@prod.com')
	
	WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), 'FastModel1')
    
} else {
	
	WebUI.setText(findTestObject('LoginPage/emailTxtFld'), 'qaautomation1scout@staging.com')
	
	WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), 'FastModel1')
}

WebUI.enhancedClick(findTestObject('LoginPage/signInBtn'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutsTab'))
WebUI.mouseOver(findTestObject('RecruitsPage/CreateFastScoutReport/ScoutEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 15)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/ScoutEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 15)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutArchiveBtn'))
WebUI.delay(2)
WebUI.mouseOver(findTestObject('RecruitsPage/CreateFastScoutReport/ScoutEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 16)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/ScoutEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 16)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ScoutArchiveBtn'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/ArchivedTab'))
WebUI.mouseOver(findTestObject('RecruitsPage/CreateFastScoutReport/ArchivedEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 15)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/ArchivedEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 15)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteScoutBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteScoutPopupBtn'))
WebUI.delay(2)
WebUI.mouseOver(findTestObject('RecruitsPage/CreateFastScoutReport/ArchivedEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
               'ScoutReportName', 16)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/ArchivedEllipsisBtn', [('ScoutReportName') : GlobalVariable.data.getValue(
                'ScoutReportName', 16)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteScoutBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/DeleteScoutPopupBtn'))
WebUI.delay(2)

//Switch to original
WebUI.switchToWindowIndex(0)
WebUI.enhancedClick(findTestObject('RecruitsPage/CreateFastScoutReport/RecruitsChckBox', [('recruitName') : GlobalVariable.data.getValue(
                'recruitName', 15)]))
WebUI.delay(2)
WebUI.closeBrowser()