import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.enhancedClick(findTestObject('LoginPage/signInBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Settings'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/manageFieldsTab'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addFieldBtn'))

WebUI.delay(2)

WebUI.setText(findTestObject('SettingsPage/ManageFields/fieldNameTxtFld'),'Rating')

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/selectStyleDrpDwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/selectStyleValue'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addField1Value'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), '2star')

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addField1Value'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), '3star')

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/SelectColorChckBox'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addField1Value'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), '4star')

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/editFieldValue'))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(1)

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), '5star')

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addFieldBtn'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/fieldNameTxtFld'), 'Status')

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/MultiSelectBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/selectStyleDrpDwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/selectStyleValue'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addField2Value'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), 'Committed')

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addField2Value'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), 'Uncommitted')

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/SelectColorChckBox'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/addField2Value'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/FieldValueTxtFld'), 'Verbal')

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue(
                'recruitName', 12)]))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/EditRecruitBtn'))

WebUI.scrollToElement(findTestObject('SettingsPage/ManageFields/ratingDrpdwn'),5)

WebUI.setText(findTestObject('SettingsPage/ManageFields/ratingDrpdwn'),'5star')

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/ratingDrpdwn'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/ratingDrpdwn'), Keys.chord(Keys.ENTER))

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue(
	'recruitName', 12)]))

WebUI.enhancedClick(findTestObject('RecruitsPage/TransferWatchlist/EditRecruitBtn'))

WebUI.scrollToElement(findTestObject('SettingsPage/ManageFields/statusDrpdwn'),5)

WebUI.setText(findTestObject('SettingsPage/ManageFields/statusDrpdwn'),'Verbal')

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/statusDrpdwn'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/statusDrpdwn'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('SettingsPage/ManageFields/statusDrpdwn'),'Committed')

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/statusDrpdwn'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/statusDrpdwn'), Keys.chord(Keys.ENTER))

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/ShowAdvancedToolsTxt'))

WebUI.setText(findTestObject('SettingsPage/ManageFields/ratingDrpdwnRecruitspage'),'5star')

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/ratingDrpdwnRecruitspage'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/ratingDrpdwnRecruitspage'), Keys.chord(Keys.ENTER))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/ClearRating'))

WebUI.delay(2)

WebUI.setText(findTestObject('SettingsPage/ManageFields/statusDrpdwnRecruitspage'),'Verbal')

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/statusDrpdwnRecruitspage'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('SettingsPage/ManageFields/statusDrpdwnRecruitspage'), Keys.chord(Keys.ENTER))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/ClearRating'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Settings'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/manageFieldsTab'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/deleteField1Value'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/deleteBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/deleteField2Value'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageFields/deleteBtn'))

WebUI.delay(1)

WebUI.closeBrowser()
