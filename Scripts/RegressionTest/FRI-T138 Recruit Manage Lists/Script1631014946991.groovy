import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.enhancedClick(findTestObject('LoginPage/signInBtn'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Settings'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/manageListsTab'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/addListBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/RuleBasedOptn'))

WebUI.setText(findTestObject('SettingsPage/ManageLists/RuleListNameTxt'), 'RuleList101')

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/SharedListWithIcon'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/StaffCheckbox'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/SelectBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/MinClassDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectMinClassFrmList'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/MaxClassDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectMaxClassFrmList'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/addListBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/FixedOptn'))

WebUI.setText(findTestObject('SettingsPage/ManageLists/FixedListNameTxt'), 'FixedList1')

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/RecruitsDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectRecruitFrmList1'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/RecruitsDrpdwnCursor'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectRecruitFrmList2'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/saveBtn'))

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectFixedList1frmDrpdwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/FixedList1DrpdwnBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectRuleList1FrmDrpdwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/rightArrowBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/rightArrowBtn'))

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Settings'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/manageListsTab'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/FixedListEditBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/RecruitsDrpdwnCursor'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectRecruitFrmList3'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/saveBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/RuleListEditBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/ClearMaxClass'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/MaxClassDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/EditMaxClassFrmList'))

WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectFixedList1frmDrpdwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/FixedList1DrpdwnBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/selectRuleList1FrmDrpdwn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/rightArrowBtn'))

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Settings'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/manageListsTab'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/FixedListDeleteBtn'))

WebUI.enhancedClick(findTestObject('SettingsPage/ManageLists/RuleListDeleteBtn'))

WebUI.delay(2)

WebUI.closeBrowser()

