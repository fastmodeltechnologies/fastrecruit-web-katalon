import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.*
import com.kms.katalon.core.webui.keyword.builtin.VerifyElementPresentKeyword as VerifyElementPresentKeyword
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('Object Repository/LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('Object Repository/LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('Object Repository/LoginPage/signInBtn'))
WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Tourneys'))

//Add Custom Tourney
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/addCustomTourney'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/customTourneyName'), 'Super Senior School Academy')
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/acronym'), 'SSA')
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/customCity'), 'Chicago')
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/customState'), 'IL')
WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/Custom_Tourney/selectDate'))
WebUI.click(findTestObject('TourneysPage/Custom_Tourney/color'))
WebUI.click(findTestObject('TourneysPage/saveBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                11)]))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/editCustomTourney'))
WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/Custom_Tourney/endDate'))
WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/manageGamesVenues'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/addVenue'))
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueName'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), 'Lincoln High School')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueAddress'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), '2000 North West')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueCity'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), 'Chicago')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueState'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), 'IL')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenuePostalCode'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), '60614')
WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/gamesTab'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/addGame'))
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddGameDate'))
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddGameTime'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), '9:00 AM')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddGameVenue'))
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddGameCourt'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), '1')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddGameTeam1'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), 'Chicago Black')
WebUI.doubleClick(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddGameTeam2'))
WebUI.setText(findTestObject('TourneysPage/Custom_Tourney/manageTourneyAddVenueNameTxtFld'), 'Chicago Orange')
WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/closeIcon'))
WebUI.delay(5)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/TourneysPage/assignStaffBtn'), 0)) {
    WebUI.comment('Found Assign Staff Btn')

    WebUI.scrollToElement(findTestObject('TourneysPage/assignStaffBtn'), 5)

    WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/assignStaffBtn'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue(
                    'assignStaff', 9)]), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/saveBtn'), FailureHandling.CONTINUE_ON_FAILURE)
} else {
    WebUI.comment('Unable to find assign staff Btn')
}

if (WebUI.verifyElementPresent(findTestObject('Object Repository/TourneysPage/assignRecruitsBtn'), 0)) {
    WebUI.comment('Found Assign recruits Btn')

    WebUI.delay(2)

    WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/assignRecruitsBtn'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/selectRecruitsDrpdwn'))

    WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitFrmList', [('recruitName') : GlobalVariable.data.getValue(
                    'assignRecruit', 11)]))

    WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/saveBtn'))

    WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/selectTourneyTeamDrpdwn'))

    WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/selectCustomTourneyTeamFrmList', [('customTourneyTeam') : GlobalVariable.data.getValue(
                    'customTourneyTeam', 11)]))
} else {
    WebUI.comment('Recruit already Present')
}

WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/myRecruitGamesTab'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/TourneysPage/gamesTabRow1'), 45)

gameDate = WebUI.getText(findTestObject('Object Repository/TourneysPage/gamesDateRow1'))

WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Schedule'))

String[] dateSplit = gameDate.split('/')

WebUI.verifyElementPresent(findTestObject('SchedulePage/calendarFromIcn'), 5)

WebUI.enhancedClick(findTestObject('SchedulePage/calendarFromIcn'))

WebUI.enhancedClick(findTestObject('SchedulePage/calendarYearBtn'))

//Month Selector
String a = '\'react-calendar__tile react-calendar__year-view__months__month\''

String month = ((('//button[@class=' + a) + '][') + (dateSplit[0])) + ']'

TestObject to = new TestObject('monthObj')

to.addProperty('xpath', ConditionType.EQUALS, month)

WebUI.enhancedClick(to)

//Date Selector
String b = '\'react-calendar__tile\''

String c = '\'neighboringMonth\''

String date = ((((('(//button[starts-with(@class,' + b) + ') and @class[not(contains(.,') + c) + '))]])[') + (dateSplit[
1])) + ']'

TestObject to2 = new TestObject('dateObj')

to2.addProperty('xpath', ConditionType.EQUALS, date)

println(dateSplit[1])

WebUI.delay(3)

WebUI.enhancedClick(to2)

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.COMMAND, 'a'))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.BACK_SPACE))

WebUI.delay(2)

WebUI.setText(findTestObject('SchedulePage/calendarDateTxt2'), '1/10/2022')

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('SchedulePage/calendarDateTxt2'), Keys.chord(Keys.ESCAPE))

WebUI.enhancedClick(findTestObject('TourneysPage/BallerTv/assignStaffChckBox2'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('SchedulePage/schedueListRow1'), 3)

WebUI.enhancedClick(findTestObject('Object Repository/TopNavigationMenu/Tourneys'))

WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                11)]))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/editCustomTourney'))

WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/deleteBtn'))

WebUI.delay(3)

WebUI.getText(findTestObject('TourneysPage/Custom_Tourney/confirmBtn'))

WebUI.verifyElementClickable(findTestObject('TourneysPage/Custom_Tourney/confirmBtn'))

WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/confirmBtn'))

WebUI.delay(3)

WebUI.closeBrowser()

