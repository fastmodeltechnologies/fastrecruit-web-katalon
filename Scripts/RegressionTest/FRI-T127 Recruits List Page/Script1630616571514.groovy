import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/selectQATop20FromList'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/QATop20DrpdwnBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/selectAllRecruitsFromList'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/CLASSUpperArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/CLASSDownArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/LASTCONTACTEDUpperArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/LASTCONTACTEDDownArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/HEIGHTUpperArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/HEIGHTDownArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/NAMEArrowIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/ALLDrpdwnBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/select2021FromList'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/2021DrpdwnBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/selectAllFromList'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/EmailBtn'))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/ListPage/EmailLinkTxt'), 0)
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/SocialBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/SocialLinkTxt'))
WebUI.switchToWindowIndex(1)
WebUI.enableSmartWait()
assert WebUI.getUrl().contains('https://twitter.com')
//Switch to original
WebUI.switchToWindowIndex(0)
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/PhoneBtn'))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/ListPage/PhoneNumTxt'), 0)
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/PaginationDrpdwnBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/select10PerPageFromList'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/ShowAdvancedToolsTxt'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Miles James')
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : 'Miles James']), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/selectCheckboxBtn', [('recruitname') : 'Miles James']))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/ListPage/PrintSelectedBtn'), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitSearchTxtFld'))
WebUI.sendKeys(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), Keys.chord(Keys.END, Keys.SHIFT, Keys.ARROW_UP, Keys.BACK_SPACE))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 12))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 12)]), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 12)]))
WebUI.mouseOver(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedSetRow'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/recruitProfileSetBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/selectRecruiterDrpdwnBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedRecruiterSelectFromDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn (1)'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/RecruitProfileRightArrow'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/RecruitProfileLeftArrow'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 12)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesEditIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/deleteBtnNotes'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/confirmBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))
WebUI.refresh()
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Josh')
WebUI.delay(2)
WebUI.closeBrowser()