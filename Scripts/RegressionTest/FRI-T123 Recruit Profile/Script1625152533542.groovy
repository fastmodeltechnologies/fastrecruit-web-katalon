import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.enhancedClick(findTestObject('LoginPage/signInBtn'))
WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 60)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 19))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 19)]), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue('recruitName', 19)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/top20BadgeIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/selectQATop20FromList'))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue('recruitName', 19)]),5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue('recruitName', 19)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/top20BadgeIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/closeBtn'))
WebUI.refresh()
WebUI.verifyElementNotPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue('recruitName', 19)]),3)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/selectAllRecruitsFromList'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue('recruitName', 19)]))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/Tasks/tasksBtn'), 0)
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/tasksBtn'), FailureHandling.STOP_ON_FAILURE)
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/addRecruitTaskBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/assignStaffDrpdwn'))
WebUI.scrollToElement(findTestObject('RecruitsPage/Tasks/selectStaffFromList', [('assignStaff') : (GlobalVariable.data).getValue('assignStaff', 19)]), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/selectStaffFromList', [('assignStaff') : (GlobalVariable.data).getValue('assignStaff', 19)]))
WebUI.setText(findTestObject('RecruitsPage/Tasks/addInstructionTxtFld'), 'Automation Task')
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/saveBtn'))
WebUI.delay(3)
WebUI.verifyElementText(findTestObject('RecruitsPage/Tasks/taskDescriptionTxt'), 'Automation Task', FailureHandling.OPTIONAL)
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/editTasksEllipsisBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/editTaskBtn'))
WebUI.setText(findTestObject('RecruitsPage/Tasks/addInstructionTxtFld'), ' updated')
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/saveBtn'))
WebUI.delay(3)
WebUI.verifyElementText(findTestObject('RecruitsPage/Tasks/taskDescriptionTxt'), 'Automation Task updated', FailureHandling.OPTIONAL)
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/editTasksEllipsisBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/Tasks/deleteTaskBtn'))
WebUI.delay(3)
WebUI.verifyElementText(findTestObject('RecruitsPage/Tasks/noUpcomingTaskTxt'), 'You have no upcoming tasks related to this recruit', FailureHandling.OPTIONAL)
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/addNoteBtn'))
WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesDescriptionTxtFld'), 'Automation Notes')
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesSaveBtn'))
WebUI.verifyElementText(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesVerifyText'), 'Automation Notes', FailureHandling.OPTIONAL)
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesEditIcon'))
WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesDescriptionTxtFld'), 'Automation Notes ')
WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesDescriptionTxtFld'), 'Automation Notes updated')
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesSaveBtn'))
WebUI.verifyElementText(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesVerifyText'), 'Automation Notes updated', FailureHandling.OPTIONAL)
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesEditIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesDeleteIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videosTab'))
WebUI.delay(3)
def imgDir = RunConfiguration.getProjectDir() + '/Data Files/Videos/SampleVideo_1MB.mp4'
WebUI.uploadFile(findTestObject('RecruitsPage/RecruitProfileView/Videos/uploadVideo'), imgDir)
WebUI.waitForElementVisible(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoImageIcn'), 60)
WebUI.verifyElementPresent(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoTitleVerifyText'), 0)
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoImageIcn'))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoPlayer'), 0)
WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoTitleTxtFld'), Keys.chord(Keys.END, Keys.SHIFT, Keys.ARROW_UP, Keys.BACK_SPACE))
WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoTitleTxtFld'), 'SampleVideo_1MB.mp4 updated')
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoExitBtn'))
WebUI.delay(3)
WebUI.verifyElementText(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoTitleVerifyText'), 'SampleVideo_1MB.mp4 updated')
WebUI.mouseOver(findTestObject('RecruitsPage/RecruitProfileView/Videos/recruitVideoRow1'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoDeleteIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoConfirmDeleteBtn'))
WebUI.delay(2)
WebUI.verifyElementText(findTestObject('RecruitsPage/RecruitProfileView/Videos/videosNotAvailableVerifyText'), 'This recruit has no videos')
WebUI.mouseOver(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedSetRow'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/recruitProfileSetBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedRecruiterDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastContactedRecruiterSelectFromDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn (1)'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Notes/notesEditIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/ListPage/deleteBtnNotes'))
WebUI.enhancedClick(findTestObject('TourneysPage/Custom_Tourney/confirmBtn'))
schoolName = WebUI.getText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolNameLink'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolNameLink'))
WebUI.verifyElementText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/schoolNameHeaderTxt'), schoolName)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/logoutCaret'))
WebUI.enhancedClick(findTestObject('TopNavigationMenu/logoutBtn'))
//WebUI.navigateToUrl('https://id-staging.fastmodelsports.com/authorize?response_type=token&client_id=xgPWUZ6j3Jr1L3bESSCm-qa&redirect_uri=https%3A%2F%2Ffastrecruit-qa.fastmodelsports.com')
WebUI.closeBrowser()