import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('LoginPage/signInBtn'))

WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 60)

WebUI.click(findTestObject('TopNavigationMenu/Recruits'))

WebUI.click(findTestObject('RecruitsPage/List/addRecruitBtn'))

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/firstNameTxtFld'), 'Josh')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/lastNameTxtFld'), 'Allen')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/classTxtFld'), '2023')

WebUI.click(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/heightTxtFld'))

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/heightTxtFld'), '69')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/birthdateTxtFld'), '1/1/2005')

WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/birthdateLabel'))

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), 'highland park')

WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolTxtFld'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolCoachNameTxtFld'), 'Coach Miller')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/highSchoolCoachPhoneTxtFld'), '8474713163')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/clubTeamTxtFld'), 'Mac Irvin Fire')

WebUI.setText(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/clubTeamNumberTxtFld'), '10')

WebUI.click(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))

WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), 'Josh Allen')

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveRecruitBtn'))

WebUI.click(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))

WebUI.click(findTestObject('RecruitsPage/List/archiveListBtn'))

WebUI.delay(5)
//WebUI.verifyElementVisible(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveDeleteBtn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivePopupDeleteBtn'))

WebUI.closeBrowser()

