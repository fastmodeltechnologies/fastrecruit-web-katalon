import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.*
import com.kms.katalon.core.webui.keyword.builtin.VerifyElementPresentKeyword as VerifyElementPresentKeyword
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Tourneys'))
WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                9)]))

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignStaffBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/AssignNotPresent'))
    WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue(
                    'assignStaff', 9)]))
    WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
}

//WebUI.scrollToElement(findTestObject('TourneysPage/assignStaffBtn'), 5)
WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffBtn'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue('assignStaff', 
                9)]))
WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
WebUI.delay(2)

if (WebUI.waitForElementNotPresent(findTestObject('TourneysPage/assignRecruitsBtn'), 0)) {
    WebUI.enhancedClick(findTestObject('TourneysPage/DeleteAssignRecruit'))

    WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))
}

WebUI.enhancedClick(findTestObject('TourneysPage/assignRecruitsBtn'))
WebUI.enhancedClick(findTestObject('Object Repository/TourneysPage/selectRecruitsDrpdwn'))
WebUI.enhancedClick(findTestObject('TourneysPage/selectRecruitFrmList', [('recruitName') : GlobalVariable.data.getValue(
					'assignRecruit', 9)]))
WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamDrpdwn'))
WebUI.enhancedClick(findTestObject('TourneysPage/selectTourneyTeamFrmList', [('tourneyTeam') : GlobalVariable.data.getValue(
                'tourneyTeam', 9)]))
WebUI.enhancedClick(findTestObject('TourneysPage/MyRecruitGamesTab'))
WebUI.verifyElementPresent(findTestObject('TourneysPage/gamesTabRow1'), 0)
gameDate = WebUI.getText(findTestObject('TourneysPage/gamesDateRow1'))
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Schedule'))
String[] dateSplit = gameDate.split('/')
WebUI.verifyElementPresent(findTestObject('SchedulePage/calendarFromIcn'), 5)
WebUI.enhancedClick(findTestObject('SchedulePage/calendarFromIcn'))
WebUI.enhancedClick(findTestObject('SchedulePage/calendarYearBtn'))

//Month Selector 
String a = '\'react-calendar__tile react-calendar__year-view__months__month\''
String month = ((('//button[@class=' + a) + '][') + (dateSplit[0])) + ']'
TestObject to = new TestObject('monthObj')
to.addProperty('xpath', ConditionType.EQUALS, month)
WebUI.enhancedClick(to)

//Date Selector
String b = '\'react-calendar__tile\''
String c = '\'neighboringMonth\''
String date = ((((('(//button[starts-with(@class,' + b) + ') and @class[not(contains(.,') + c) + '))]])[') + (dateSplit[
1])) + ']'
TestObject to2 = new TestObject('dateObj')
to2.addProperty('xpath', ConditionType.EQUALS, date)
println(dateSplit[1])
WebUI.delay(2)
WebUI.enhancedClick(to2)
WebUI.verifyElementPresent(findTestObject('SchedulePage/schedueListRow1'), 0)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Tourneys'))
WebUI.enhancedClick(findTestObject('TourneysPage/tourneyNameFromList', [('tourneyName') : GlobalVariable.data.getValue('tourneyName', 
                9)]))
WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffEditBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/assignStaffChckBox', [('staffName') : GlobalVariable.data.getValue('assignStaff', 
                9)]), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.enhancedClick(findTestObject('TourneysPage/saveBtn'))
WebUI.delay(1)
WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitBtn'))
WebUI.enhancedClick(findTestObject('TourneysPage/deleteRecruitConfirmBtn'))
//WebUI.enhancedClick(findTestObject('TopNavigationMenu/logoutCaret'))
//
//WebUI.click(findTestObject('TopNavigationMenu/logoutBtn'))
WebUI.closeBrowser()