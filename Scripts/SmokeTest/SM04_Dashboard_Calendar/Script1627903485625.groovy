import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('LoginPage/signInBtn'))

WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Dashboard'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/calendarLeftToggleBtn'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/calendarRightToggleBtn'))

WebUI.delay(5)

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/showDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/myCalendarCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/myCalendarCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/teamCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/teamCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/tasksCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/tasksCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/ncaaCalendarCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/ncaaCalendarCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/myLivePeriodCheckbox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/myLivePeriodCheckbox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/showDrpdwnBtn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/addNewBtn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/eventNameTxtFld'))

WebUI.sendKeys(findTestObject('DashboardPage/Calendar/Add Event modal/eventNameTxtFld'), 'Test Event1')

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/locationTxtFld'))

WebUI.sendKeys(findTestObject('DashboardPage/Calendar/Add Event modal/locationTxtFld'), '444 N. Michigan Ave')

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/selectLocationDrpdwn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/recruitsDrpdwn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/selectRecruitDrpdwn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/attendeesDrpdwn'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/attendeesCheckBox'))

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/gameNoteTxtFld'))

WebUI.sendKeys(findTestObject('DashboardPage/Calendar/Add Event modal/gameNoteTxtFld'), 'Arrive early')

WebUI.enhancedClick(findTestObject('DashboardPage/Calendar/Add Event modal/saveBtn'))

WebUI.delay(2)

WebUI.verifyTextPresent('Test Event1', false)

WebUI.closeBrowser()

