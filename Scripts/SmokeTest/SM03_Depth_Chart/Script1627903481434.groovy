import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.environment)

WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)

WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)

WebUI.click(findTestObject('LoginPage/signInBtn'))

WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 20)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Dashboard'))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/depthChartSeasonDrpdwn'))

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/depthSeasonDrpdwnValue', [('seasonDrpDwn') : (GlobalVariable.data).getValue("seasonDrpDwn", 7)]))

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/editDepthChartBtn'))

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/recruitSelectCheckBox', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 7)]))

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/saveDepthChartBtn'))

WebUI.delay(2)

WebUI.verifyTextNotPresent((GlobalVariable.data).getValue("recruitName", 7), false)

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/editDepthChartBtn'))

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/recruitSelectCheckBox', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 7)]))

WebUI.enhancedClick(findTestObject('DashboardPage/DepthChart/saveDepthChartBtn'))

WebUI.verifyTextPresent((GlobalVariable.data).getValue("recruitName", 7), false)

WebUI.delay(1)

WebUI.closeBrowser()

