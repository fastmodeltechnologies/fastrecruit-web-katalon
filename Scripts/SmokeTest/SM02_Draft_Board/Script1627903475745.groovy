import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


WebUI.openBrowser(GlobalVariable.environment)
//WebUI.setViewPortSize(1440, 841)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 60)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Dashboard'))
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/seasonDrpdwnBtn'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftSeasonDrpdwnValue', [('seasonDrpDwn') : (GlobalVariable.data).getValue("seasonDrpDwn", 5)]))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/blankPGMouseOver'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/blankPGMouseOver'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/selectPlayerFromDrpdwn', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 5)]))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.delay(1)
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/alertsIcn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/closeBtn'))
WebUI.verifyElementVisible(findTestObject('DashboardPage/DraftBoard/alertsBellIndicator'))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/alertsIcn'))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/closeBtn'))
WebUI.delay(1)
WebUI.verifyElementNotPresent(findTestObject('DashboardPage/DraftBoard/alertsBellIndicator'), 0)
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/deleteBtnPG1'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/seasonDrpdwnBtn'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftSeasonDrpdwnValue', [('seasonDrpDwn') : (GlobalVariable.data).getValue("seasonDrpDwn", 5)]))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/blankPGMouseOver'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/blankPGMouseOver'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/selectPlayerFromDrpdwn', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 5)]))
WebUI.delay(1)
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/blankPGMouseOver'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/blankPG1DraftBoard'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/recruitSearchTxtFld'))
WebUI.sendKeys(findTestObject('DashboardPage/DraftBoard/recruitSearchTxtFld'), (GlobalVariable.data).getValue("assignRecruit", 5))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/player2SelectDrpdwn'))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG2'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/deleteBtnPG2'))
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/deleteBtnPG1'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/draftBoardLink'))
WebUI.mouseOver(findTestObject('RecruitsPage/DraftBoard/blankPG1DraftBoard'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/blankPG1DraftBoard'))
WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/selectPlayerFromDrpdwn', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 5)]))

WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/recruitColorDropperIcon'))
WebUI.delay(3)
WebUI.verifyElementVisible(findTestObject('RecruitsPage/DraftBoard/yellowPalleteBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/yellowPalleteBtn'))
WebUI.comment("Yellow Pallete Btn selected : " + WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color'))
WebUI.delay(3)
//WebUI.comment(WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color'))
assert (WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color')).equalsIgnoreCase('rgb(255, 212, 0)')
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/recruitColorDropperIcon'))
WebUI.delay(3)
WebUI.verifyElementVisible(findTestObject('RecruitsPage/DraftBoard/greenPalleteBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/greenPalleteBtn'))
WebUI.comment("Green Pallete selected")
WebUI.delay(1)
WebUI.comment(WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color'))
WebUI.delay(3)
assert (WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color')).equalsIgnoreCase('rgb(0, 128, 0)')

//WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/recruitColorDropperIcon'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/colorPalleteEditBtn'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/addColorPlusBtn'))
//
//WebUI.clearText(findTestObject('RecruitsPage/DraftBoard/hexColorTxtFld'))
//
//WebUI.sendKeys(findTestObject('RecruitsPage/DraftBoard/hexColorTxtFld'), '4832a8')
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/manageColorsCloseBtn'))
//
//WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/recruitColorDropperIcon'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/customColor1'))
//
//WebUI.delay(1)
//WebUI.comment("Custom Pallete selected")
//WebUI.comment(WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color'))
//assert (WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color')).equalsIgnoreCase('rgb(72, 50, 168)')
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/recruitColorDropperIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/resetColorBtn'))
WebUI.delay(1)
WebUI.comment("Pallete reset")
assert (WebUI.getCSSValue(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'),'border-color')).equalsIgnoreCase('rgb(204, 204, 204)')
WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))

//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/recruitColorDropperIcon'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/colorPalleteEditBtn'))
//
//WebUI.mouseOver(findTestObject('RecruitsPage/DraftBoard/customColor1'))
//
//WebUI.waitForElementPresent(findTestObject('RecruitsPage/DraftBoard/deleteCustomColorBtn'), 0)
//
//if (WebUI.verifyElementPresent(findTestObject('RecruitsPage/DraftBoard/deleteCustomColorBtn'), 0)) {
//	WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/deleteCustomColorBtn'))
//}
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/deleteCustomColorBtn'))
//
//WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/manageColorsCloseBtn'))
//
//WebUI.mouseOver(findTestObject('DashboardPage/DraftBoard/draftBoardPG1'))

WebUI.enhancedClick(findTestObject('DashboardPage/DraftBoard/deleteBtnPG1'))
WebUI.delay(1)
WebUI.closeBrowser()