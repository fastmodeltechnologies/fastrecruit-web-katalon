import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


  WebUI.openBrowser('')
  
  WebUI.navigateToUrl(GlobalVariable.environment, FailureHandling.STOP_ON_FAILURE)
  
  WebUI.maximizeWindow()
  
  WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
  
  WebUI.setText(findTestObject('LoginPage/passwordTxtFld'),
  GlobalVariable.password)
  
  WebUI.click(findTestObject('LoginPage/signInBtn'))
  
  WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'),
  60)
  
  WebUI.click(findTestObject('TopNavigationMenu/Recruits'))
  
  WebUI.click(findTestObject('RecruitsPage/List/addRecruitBtn'))
  
  WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkToFastScoutChckBox'))
  
  WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/divisionDrpdwn'))
  
  WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/divisionName', [('divisionName') : (GlobalVariable.data).getValue("divisionName", 1)]))
  
  WebUI.scrollToElement(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonDrpdwn'),5)
  
  WebUI.delay(2)
  
  WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonDrpdwn'))
  
  WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonName', [('linkSeasonName') : (GlobalVariable.data).getValue('linkSeasonName', 1)]))
  
  WebUI.delay(2)
  
  WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkTeamDropdown'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkTeamName', [('linkTeamName') : (GlobalVariable.data).getValue("linkTeamName", 1)]))
 
WebUI.delay(1)

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkPlayerDrpdwn'))

WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkPlayerName',[('linkPlayerName') : (GlobalVariable.data).getValue("linkPlayerName", 1)]))

WebUI.click(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))

WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), (GlobalVariable.data).getValue("linkPlayerName", 1))

WebUI.delay(3)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveRecruitBtn'))

WebUI.delay(1)

WebUI.click(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))

WebUI.delay(1)

WebUI.click(findTestObject('RecruitsPage/List/archiveListBtn'))

WebUI.delay(1)

WebUI.click(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))

WebUI.delay(1)

WebUI.click(findTestObject('RecruitsPage/List/archiveDeleteBtn'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivePopupDeleteBtn'))

WebUI.delay(2)

WebUI.closeBrowser()

