import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email2)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Settings'), 60)
//WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/addRecruitBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkToFastScoutChckBox'))
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/divisionDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/divisionName', [('divisionName') : GlobalVariable.data.getValue('divisionName', 3)]))
WebUI.scrollToElement(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonDrpdwn'), 5)
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkSeasonName', [('linkSeasonName') : GlobalVariable.data.getValue('linkSeasonName', 3)]))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkTeamDropdown'))
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkTeamName', [('linkTeamName') : GlobalVariable.data.getValue('linkTeamName', 3)]))
WebUI.delay(2)
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkPlayerDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/LinkToFastScout/linkPlayerName', [('linkPlayerName') : GlobalVariable.data.getValue('linkPlayerName', 3)]))
WebUI.click(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/saveBtn'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 3))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 3)]), 5)
WebUI.mouseOver(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 3)]))
//WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 3)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/RecruitInfo/fastScoutTeamNameLink(Prospect Profile)'))
WebUI.switchToWindowIndex(1)
WebUI.enableSmartWait()
WebUI.delay(3)
assert WebUI.getUrl().contains('https://fastscout')
WebUI.delay(8)
WebUI.getText(findTestObject('Object Repository/RecruitsPage/LinkToFastScout/scoutTeamTitleText'))
WebUI.switchToWindowIndex(0)
WebUI.click(findTestObject('RecruitsPage/List/archiveRecruitBtn'))
WebUI.click(findTestObject('RecruitsPage/List/allRecruitsDrpdwn'))
WebUI.click(findTestObject('RecruitsPage/List/archiveListBtn'))
WebUI.delay(1)
WebUI.waitForElementClickable(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'), 10)
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivedListEllipsisBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archiveDeleteBtn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/List/archivePopupDeleteBtn'))
WebUI.closeBrowser()