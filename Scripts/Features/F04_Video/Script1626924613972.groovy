import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'), GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'), 60)
WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))
WebUI.setText(findTestObject('RecruitsPage/List/recruitSearchTxtFld'), GlobalVariable.data.getValue('recruitName', 4))
WebUI.verifyElementPresent(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : GlobalVariable.data.getValue('recruitName', 4)]), 5)
WebUI.enhancedClick(findTestObject('RecruitsPage/List/recruitNameFromGrid', [('recruitName') : (GlobalVariable.data).getValue("recruitName", 4)]))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videosTab'))
def imgDir = RunConfiguration.getProjectDir() + '/Data Files/Videos/SampleVideo_1MB.mp4'
WebUI.uploadFile(findTestObject('RecruitsPage/RecruitProfileView/Videos/uploadVideo'), imgDir)
WebUI.verifyElementPresent(findTestObject('RecruitsPage/RecruitProfileView/Videos/recruitVideoRow1'), 0)
WebUI.delay(2)
WebUI.mouseOver(findTestObject('RecruitsPage/RecruitProfileView/Videos/recruitVideoRow1'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoDeleteIcon'))
WebUI.enhancedClick(findTestObject('RecruitsPage/RecruitProfileView/Videos/videoConfirmCancelBtn'))
WebUI.verifyElementNotPresent(findTestObject('RecruitsPage/RecruitProfileView/Videos/recruitVideoRow1'), 0)
WebUI.enhancedClick(findTestObject('RecruitsPage/DraftBoard/closeBtn'))
WebUI.closeBrowser()