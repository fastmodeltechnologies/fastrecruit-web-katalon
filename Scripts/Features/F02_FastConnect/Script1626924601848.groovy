import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.openBrowser(GlobalVariable.environment)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('LoginPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('LoginPage/passwordTxtFld'),GlobalVariable.password)
WebUI.click(findTestObject('LoginPage/signInBtn'))
WebUI.waitForElementVisible(findTestObject('TopNavigationMenu/Dashboard'),60)

WebUI.enhancedClick(findTestObject('TopNavigationMenu/Recruits'))
WebUI.enhancedClick(findTestObject('RecruitsPage/FastConnect/fastConnectTab'))
WebUI.enhancedClick(findTestObject('RecruitsPage/FastConnect/fastConnectListDrpdwn'))
WebUI.enhancedClick(findTestObject('RecruitsPage/FastConnect/allRecruitslist', [('fastConnectDrpdwn') : (GlobalVariable.data).getValue("fastConnectDrpdwn", 2)]))
WebUI.delay(10)


WebUI.verifyElementVisible(findTestObject('RecruitsPage/FastConnect/fastconnectPost1'), FailureHandling.STOP_ON_FAILURE)
WebUI.closeBrowser()